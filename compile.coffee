fs = require 'fs'
mallow = require 'mallow'

bundle = mallow.new(__dirname)
file = process.argv[2] or 'mallow.js'

bundle.compile (err, output) ->
  if err
    console.error(err)
    process.exit(1)
  else
    fs.writeFile file, output
