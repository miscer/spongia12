love = require 'lovejs'
bond = require 'bond'

class Loading extends bond.components.Stage
  constructor: ->
    @assets =
      background: love.assets.addImage('images/loading/background.png')
      fluid: love.assets.addImage('images/loading/fluid.png')
      progressbar: love.assets.addImage('images/loading/progressbar.png')

    @quad = love.graphics.newQuad()
    @loaded = false

    love.assets.load(@init)

  init: =>
    @loaded = true

    @images =
      background: love.graphics.newImage(@assets.background)
      fluid: love.graphics.newImage(@assets.fluid)
      progressbar: love.graphics.newImage(@assets.progressbar)

  draw: ->
    if not @loaded
      return

    progress = love.assets.getProgress()
    ratio = progress.loaded / progress.total

    love.graphics.draw(@images.background, 0, 0)

    @quad.setViewport(0, 0, 490 * ratio, 22)
    love.graphics.drawq(@images.progressbar, @quad, 234, 450)

    @quad.setViewport(Math.floor(ratio * 10) * 50, 0, 50, 62)
    love.graphics.drawq(@images.fluid, @quad, 438, 274)

  load: (cb) ->
    love.assets.load(cb)

module.exports = Loading
