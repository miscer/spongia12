love = require 'lovejs'
bond = require 'bond'

game = require './game'
state = require './state'

class DeathScreen extends bond.components.Stage
  constructor: ->
    @asset = love.assets.addImage('images/death/background.png')
    @audio = love.assets.addAudio('other/music/death.mp3')
    love.assets.addImage('images/death/buttons.png')

    @buttons =
      menu: bond.gui.newButton('menu', 30, 548, 'death-menu')
      continue: bond.gui.newButton('continue', 666, 548, 'death-continue')

    for name, button of @buttons
      button.on('click', @[name])

  init: ->
    @background = love.graphics.newImage(@asset)
    @audio = @audio.getContent()

  enter: ->
    for name, button of @buttons
      bond.gui.add(button)

    @audio.play()

  leave: ->
    for name, button of @buttons
      bond.gui.remove(button)

    @audio.pause()
    @audio.currentTime = 0

  draw: ->
    love.graphics.draw(@background, 0, 0)

  menu: =>
    game.menu()

  continue: =>
    game.level(state.levels.current)

module.exports = DeathScreen
