bond = require 'bond'

Menu = require './menu'
Loading = require './loading'
DeathScreen = require './death_screen'
NormalLevel = require './level/normal'
Tutorial = require './level/tutorial'
Credits = require './credits'

# --- current ---

$current = null

exports.getCurrent = ->
  $current

setCurrent = (stage) ->
  if $current? and $current.leave?
    $current.leave()

  if stage.enter?
    stage.enter()

  $current = stage

# --- menu ---

_menu = null
exports.menu = ->
  if _menu is null
    _menu = new Menu

  if not _menu.loaded
    exports.loading ->
      _menu.init()
      exports.menu()
  else
    setCurrent(_menu)

# --- loading screen ---

_loading = null
exports.loading = (cb) ->
  if _loading is null
    _loading = new Loading

  setCurrent(_loading)
  _loading.load(cb)

# --- level ---

levelClasses =
  0: Tutorial
  1: NormalLevel
  2: NormalLevel
  3: NormalLevel
  4: NormalLevel
  5: NormalLevel
  6: NormalLevel
  7: NormalLevel
  8: NormalLevel
  9: NormalLevel
  10: NormalLevel

exports.level = (i) ->
  level = new levelClasses[i](i)

  exports.loading ->
    level.init()
    setCurrent(level)

# --- death screen ---

_deathScreen = null
exports.deathScreen = ->
  if _deathScreen is null
    _deathScreen = new DeathScreen
    exports.loading ->
      _deathScreen.init()
      setCurrent(_deathScreen)
  else
    setCurrent(_deathScreen)

_credits = null
exports.credits = ->
  if _credits is null
    _credits = new Credits
    exports.loading ->
      _credits.init()
      setCurrent(_credits)
  else
    setCurrent(_credits)
