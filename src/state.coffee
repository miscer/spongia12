love = require 'lovejs'

exports.player = 

  level: 1

  levels:
    attack: 1
    vitality: 1
    robin: 1
    speed: 1
    luck: 1

  points:
    stat: 0
    health: null
    special: null
    experience: null

  potions:
    health: 0
    special: 0

exports.levels =
  completed: []
  current: 0

exports.cards = []

isSupported = ->
  window.localStorage?

exports.save = ->
  if not isSupported()
    return

  localStorage.state = JSON.stringify(exports)

exports.load = ->
  if not isSupported()
    return

  if not localStorage.state?
    return

  state = JSON.parse(localStorage.state)

  for key, value of state
    exports[key] = value

