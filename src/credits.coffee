love = require 'lovejs'
bond = require 'bond'

IMAGES = [
  'letter1.png', 'letter2.png'
  'cred1.png', 'cred2.png', 'cred3.png', 'cred4.png', 'cred5.png'
  'cred6.png', 'cred7.png', 'cred8.png', 'cred9.png',
  'cred11.png', 'cred12.png', 'cred13.png', 'nepenthe.png'
]

class Credits extends bond.components.Stage
  constructor: ->
    @assets = []
    for image, i in IMAGES
      @assets[i] = love.assets.addImage("images/credits/#{image}")

    @current = 0

    window.addEventListener('keydown', @next)
    window.addEventListener('mousedown', @next)

  init: ->
    @images = []
    for image, i in IMAGES
      @images[i] = love.graphics.newImage(@assets[i])

  next: =>
    if @current < (@images.length - 1)
      @current++

  draw: ->
    image = @images[@current]
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()

    x = (width - image.width) / 2
    y = (height - image.height) / 2

    love.graphics.draw(image, x, y)


module.exports = Credits
