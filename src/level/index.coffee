love = require 'lovejs'
{components} = require 'bond'
Player = require './entities/player'
Map = require './components/map'
Camera = require './components/camera'
Background = require './components/background'
Dialog = require './components/dialog'
Infobox = require './components/infobox'
Triggers = require './components/triggers'
Enemies = require './components/enemies'
Robins = require './components/robins'
Platforms = require './components/platforms'
Collisions = require './components/collisions'
Fighting = require './components/fighting'
EnemyZones = require './components/enemy_zones'
Potions = require './components/potions'
Cards = require './components/cards'
Controlbar = require './components/controlbar'
PauseMute = require './components/pause_mute'

SkillBox = require './windows/skill_box'
CardBox = require './windows/card'

state = require '../state'
game = require '../game'

maps =
  0: require './maps/tutorial'
  1: require './maps/stage1'
  2: require './maps/stage2'
  3: require './maps/stage3'
  4: require './maps/stage4'
  5: require './maps/stage5'
  6: require './maps/stage6'
  7: require './maps/stage7'
  8: require './maps/stage8'
  9: require './maps/stage9'
  10: require './maps/stage10'

music = null

class Level extends components.Stage
  constructor: (@num) ->
    @loaded = false
    @paused = false

    super()

    @load()

    @player.points.on('dead', @dead)

    if not music?
      music =
        one: love.assets.addAudio('other/music/one.mp3')
        two: love.assets.addAudio('other/music/two.mp3')

  load: ->
    for name, value of state.player.levels
      @player.levels[name].value = value

    @player.levels.main = state.player.level
    @player.levels.points = state.player.points.stat

    @player.levels.trigger('update')

    points = state.player.points
    if points.health? and points.special? and points.experience?
      @player.points.health = points.health
      @player.points.special = points.special
      @player.points.experience = points.experience

    @player.points.trigger('update')

    @player.potions.health = state.player.potions.health
    @player.potions.special = state.player.potions.special

    @player.potions.trigger('update')

    state.levels.current = @num
    state.save()

  save: ->
    for name, value of state.player.levels
      state.player.levels[name] = @player.levels.get(name)

    state.player.level = @player.levels.main
    state.player.points.stat = @player.levels.points

    state.player.points.health = @player.points.health
    state.player.points.special = @player.points.special
    state.player.points.experience = @player.points.experience

    state.player.potions.health = @player.potions.health
    state.player.potions.special = @player.potions.special

    state.save()

  leave: ->
    @background.clear()
    @controlbar.clear()
    @dialog.clear()
    @pauseMute.clear()

    @music.pause()
    @music.currentTime = 0

  init: ->
    @loaded = true

    super

    if @num % 2 is 0
      @music = music.one.getContent()
    else
      @music = music.two.getContent()

    @music.loop = true
    @music.play()

  # --- components ---

  createComponentDialog: ->
    new Dialog

  createComponentMap: ->
    new Map(maps[@num])

  createComponentCollisions: ->
    new Collisions

  createComponentCamera: ->
    new Camera

  createComponentBackground: ->
    new Background(@num)

  createComponentPlayer: ->
    new Player

  createComponentEnemies: ->
    new Enemies

  createComponentRobins: ->
    new Robins

  createComponentInfobox: ->
    points = @getComponent('player').getComponent('points')
    new Infobox(points)

  createComponentTriggers: ->
    new Triggers

  createComponentPlatforms: ->
    new Platforms

  createComponentFighting: ->
    new Fighting

  createComponentEnemyZones: ->
    new EnemyZones

  createComponentPotions: ->
    new Potions

  createComponentCards: ->
    new Cards

  createComponentControlbar: ->
    new Controlbar(0, 520)

  createComponentPauseMute: ->
    new PauseMute

  createComponentSkillbox: ->
    new SkillBox

  createComponentCardbox: ->
    new CardBox

  # --- pausing ---

  isPaused: ->
    @paused

  pause: ->
    @paused = true

  resume: ->
    @paused = false

  # --- state stuff ---

  next: =>
    @save()
    game.level(@num + 1)

  prev: =>
    @save()
    game.level(@num - 1)

  dead: =>
    @player.points.set('health', @player.points.max('health'))
    game.deathScreen()

  # --- functions ---

  update: (dt) ->
    if @paused
      return

    @player.update(dt)
    @enemies.update(dt)
    @robins.update(dt)
    @triggers.update()
    @enemyZones.update()
    @cards.update(dt)
    @fighting.update(dt)
    @collisions.update()
    @camera.update()

  draw: ->
    @background.draw()

    @camera.translate()
    @map.draw()
    @potions.draw()
    @cards.draw()
    @player.draw()
    @enemies.draw()
    @robins.draw()
    @camera.restore()
    
    @controlbar.draw()
    @infobox.draw()

module.exports = Level
