love = require 'lovejs'
bond = require 'bond'

class Flying extends bond.components.Component
  constructor: (@speed) ->
    @flying = false
    @to = new bond.vectors.Vector
    @step = new bond.vectors.Vector
    @distance = new bond.vectors.Vector

    love.eventify(this)

  isFlying: ->
    @flying

  stop: ->
    @flying = false

  flyTo: (x, y, min = 20) ->
    if @to.x is x and @to.y is y
      return

    @to.update(x, y)
    @flying = true
    @min2 = min * min

    @trigger('start', x, y)

    return

  update: (dt) ->
    if not @flying
      return

    movement = @parent.movement
    pos = movement.getPosition()

    @step.vupdate(@to).vsubtract(pos)

    if @step.length2() < @min2
      movement.stop()
      @flying = false
      @trigger('end')
    else
      @step.normalize().multiply(@speed)
      movement.setVelocity(@step.x, @step.y)

    return

module.exports = Flying
