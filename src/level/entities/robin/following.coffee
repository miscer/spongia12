bond = require 'bond'

class Following extends bond.components.Component
  constructor: ->
    @player = null
    @destination = {x: null, y: null}

  attached: ->
    flying = @parent.getComponent('flying')
    flying.on('end', @changeDir)

  start: (player) ->
    @player = player

  stop: ->
    @player = null

  getDestination: ->
    dest = @player.movement.getBehindHead()
    w = @parent.movement.getAABB().getWidth()

    @destination.x = dest.x + (w / 2)
    @destination.y = dest.y

    @destination

  update: ->
    if @player? and not @parent.attacking.isAttacking()
      dest = @getDestination()
      @parent.flying.flyTo(dest.x, dest.y)

    if @player?
      @parent.movement.setDir(@player.movement.getDir())

  jumpToPlayer: ->
    if @player?
      dest = @getDestination()
      @parent.movement.setPosition(dest.x, dest.y)
      @changeDir()

  changeDir: =>
    if @player?
      @parent.drawing.fly(@player.movement.getDir())

module.exports = Following
