love = require 'lovejs'
bond = require 'bond'

class Noticing extends bond.components.Component
  constructor: ->
    @asset = love.assets.addImage('images/robin/e_mark.png')

  init: ->
    image = love.graphics.newImage(@asset)
    reel = new bond.animation.Reel(image, 28, 58)

    @anim = new bond.animation.Nonlinear(reel, [[1, 0.2], [2, 0.1], [1, 0.2]])
    @anim.end = @end

    @playing = false

  notice: (cb) ->
    @playing = true
    @cb = cb

  end: =>
    @playing = false

    if @cb?
      @cb()
      @cb = null

  update: (dt) ->
    if @playing
      @anim.update(dt)

  draw: ->
    if @playing
      pos = @parent.getPosition()
      love.graphics.draw(@anim, pos.x + 3, pos.y - 70)

module.exports = Noticing
