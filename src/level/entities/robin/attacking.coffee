bond = require 'bond'

class Attacking extends bond.components.Component
  constructor: (@speed) ->
    @attacking = false
    @blocked = false

  block: ->
    @blocked = true

  unblock: ->
    @blocked = false

  canAttack: ->
    not @isAttacking() and
    not @blocked and
    @parent.getPlayer().fighting.canAttack()

  attack: ->
    if not @canAttack()
      return

    @attacking = 'normal'
    @flyForward()

  special: ->
    if not @canAttack()
      return

    pl = @parent.getPlayer()

    if pl.points.canDoSpecialAttack()
      cost = pl.mechanics.getSpecialAttackCost()
      pl.points.subtract('special', cost)

      @attacking = 'special'
      @flyForward()

  flyForward: ->
    dir = @parent.movement.getDir()
    @parent.movement.setDimensions(34, 80)

    @parent.flying.stop()

    velx = if dir is 'left' then -@speed else @speed
    @parent.movement.setVelocity(velx, 0)

  update: ->
    if not @isAttacking()
      return

    robin = @parent
    robins = robin.parent
    level = robins.parent

    if not level.camera.isVisible(robin.getAABB())
      @attacking = false
      @jumpBack()

  hit: (enemy) ->
    @parent.getPlayer().fighting.attack(enemy, @attacking)
    @attacking = false

    @jumpBack()

  jumpBack: ->
    dir = @parent.movement.getDir()
    @parent.drawing.anims.attacking[dir].rewind()

    @parent.movement.stop()
    @parent.movement.setDimensions(34, 34)
    @parent.following.jumpToPlayer()

  isAttacking: ->
    !!@attacking

module.exports = Attacking
