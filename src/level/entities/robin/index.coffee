bond = require 'bond'

Movement = require './movement'
Flying = require './flying'
Following = require './following'
Noticing = require './noticing'
Attacking = require './attacking'
Drawing = require './drawing'

class Robin extends bond.components.Entity
  constructor: (@color) ->
    @player = null

  createComponentFlying: ->
    new Flying(200)

  createComponentFollowing: ->
    new Following

  createComponentNoticing: ->
    new Noticing
  
  createComponentAttacking: ->
    new Attacking(500)

  createComponentMovement: ->
    collisions = @parent.getLevel().getComponent('collisions')

    movement = new Movement(collisions, "robin-#{@color}")
    movement.setDimensions(34, 34)
    movement

  createComponentDrawing: ->
    new Drawing

  getPosition: ->
    @movement.getPosition()

  setPosition: (x, y) ->
    @movement.setPosition(x, y)

  getAABB: ->
    @movement.getAABB()

  getPlayer: ->
    @player

  setPlayer: (player) ->
    @player = player

module.exports = Robin
