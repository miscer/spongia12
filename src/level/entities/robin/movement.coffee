Movement = require '../common_new/movement'

class RobinMovement extends Movement
  collision: (other) ->
    if other.hasTag('enemy') and @parent.attacking.isAttacking()
      @parent.attacking.hit(other.getObject())

module.exports = RobinMovement
