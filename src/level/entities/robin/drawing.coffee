love = require 'lovejs'
bond = require 'bond'

reel = (asset, w, h) ->
  image = love.graphics.newImage(asset)
  new bond.animation.Reel(image, w, h)

anim = (reel, interval, frames) ->
  new bond.animation.Animation(reel, interval, frames)

class Drawing extends bond.components.Component
  attached: ->
    @assets =
      flying: love.assets.addImage("images/robin/#{@parent.color}.flying.png")
      attacking: love.assets.addImage("images/robin/#{@parent.color}.attacking.png")

  init: ->
    @reels =
      flying: reel(@assets.flying, 34, 34)
      attacking: reel(@assets.attacking, 34, 80)

    @anims =
      sitting:
        left: anim(@reels.flying, 1, [1])
        right: anim(@reels.flying, 1, [13])
      flying:
        left: anim(@reels.flying, 0.1, [1..6])
        right: anim(@reels.flying, 0.1, [13..18])
      talking:
        left: anim(@reels.flying, 0.1, [7..12])
        right: anim(@reels.flying, 0.1, [19..24])
      attacking:
        left: new bond.animation.Nonlinear(@reels.attacking, [[12, 0.1], [11, 0.1], [10, 0.1], [9, 0.1], [8, 0.1], [7, Infinity]])
        right: new bond.animation.Nonlinear(@reels.attacking, [[1, 0.1], [2, 0.1], [3, 0.1], [4, 0.1], [5, 0.1], [6, Infinity]])
    
    @active = @anims.flying.right

  sit: (dir) ->
    @parent.movement.setDir(dir)
    @active = @anims.sitting[dir]

  talk: (dir) ->
    @parent.movement.setDir(dir)
    @active = @anims.talking[dir]

  fly: (dir) ->
    @parent.movement.setDir(dir)
    @active = @anims.flying[dir]

  attack: (dir) ->
    @parent.movement.setDir(dir)
    @active = @anims.attacking[dir]

  update: (dt) ->
    fl = @parent.flying
    at = @parent.attacking

    if at.isAttacking()
      @attack(@parent.movement.getDir())
    else if fl.isFlying()
      @fly(@parent.movement.getDir())

    @active.update(dt)

  draw: ->
    pos = @parent.getPosition()
    love.graphics.draw(@active, pos.x, pos.y)

module.exports = Drawing
