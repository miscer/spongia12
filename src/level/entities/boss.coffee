Enemy = require './enemy'

class Boss extends Enemy
  CARDS =
    jose: 1
    unicorn: 2
    dwarf: 3
    doll: 4

  init: ->
    super
    
    @points.once('dead', @dropCard)

  dropCard: =>
    if not CARDS[@name]
      return

    aabb = @getAABB()
    x = aabb.min.x + aabb.getWidth() / 2
    y = aabb.max.y - 100
    num = CARDS[@name]

    @parent.getLevel().cards.spawn(num, x, y)

module.exports = Boss
