love = require 'lovejs'
bond = require 'bond'

class Fighting extends bond.components.Component
  constructor: ->
    @target = null
    @timer = 0

    love.eventify(this)

  getTarget: ->
    @target

  attack: (entity, attack) ->
    @timer = 0
    
    @trigger('attack', entity)

    attack ?= @parent.mechanics.getAttack()
    entity.fighting.hurt(attack, @parent)

  hurt: (attack, attacker) ->
    damage = @parent.mechanics.getDamage(attack)
    @trigger('hurt', damage, attacker)

  update: (dt) ->
    if @timer < 10
      @timer += dt

  canAttack: ->
    @timer >= @parent.mechanics.getAttackSpeed()

module.exports = Fighting
