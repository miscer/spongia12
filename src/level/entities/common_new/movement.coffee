love = require 'lovejs'
bond = require 'bond'

class Movement extends bond.components.Component
  constructor: (collisions, tag) ->
    @collider = new bond.collisions.Collider
    @collider.addTag(tag)
    @collider.collision = @collision

    collisions.add(@collider)

    @velocity = new bond.vectors.Vector
    @step = new bond.vectors.Vector

    @dir = 'right'

    love.eventify(this)

  attached: ->
    @collider.setObject(@parent)

  getPosition: ->
    @collider.aabb.min

  setPosition: (x, y) ->
    @collider.aabb.moveTo(x, y)

  getAABB: ->
    @collider.aabb

  setDimensions: (w, h) ->
    @collider.aabb.resize(w, h)

  getVelocity: ->
    @velocity

  setVelocity: (x, y) ->
    @velocity.update(x, y)
    @updateDir()

  move: (x, y) ->
    @velocity.add(x, y)
    @updateDir()

  stop: ->
    @setVelocity(0, 0)

  getDir: ->
    @dir

  setDir: (dir) ->
    @dir = dir

  updateDir: ->
    if @velocity.x > 0
      @dir = 'right'
    else if @velocity.x < 0
      @dir = 'left'

  update: (dt) ->
    @step.vupdate(@velocity).multiply(dt)
    @collider.update(@step.x, @step.y)

  collision: (other) =>
    if other.hasTag('solid')
      aabb = @collider.aabb
      resolve = other.aabb.resolve(aabb)

      aabb.move(resolve.x, resolve.y)
      @collider.stop()

      if resolve.x < 0
        @trigger('right', other)
      else if resolve.x > 0
        @trigger('left', other)

      if resolve.y < 0
        @trigger('down', other)
      else if resolve.y > 0
        @trigger('up', other)

module.exports = Movement
