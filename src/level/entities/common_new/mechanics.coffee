bond = require 'bond'

err = ->
  throw new Error("not implemented")

class Mechanics extends bond.components.Component
  getAttack: ->
    err()

  getMaxHP: ->
    err()

  getAttackSpeed: ->
    err()

  getDefense: ->
    err()

  getDamage: (atk) ->
    Math.max(0, atk - @getDefense())

module.exports = Mechanics
