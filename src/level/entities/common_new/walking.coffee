love = require 'lovejs'
bond = require 'bond'

class Walking extends bond.components.Component
  constructor: (@speed) ->
    @dir = 0

    love.eventify(this)

  start: (dir) ->
    if @dir is dir
      return
    
    if @dir isnt 0
      @stop(@dir)

    @trigger('start', dir)

    @dir = dir
    @parent.movement.move(dir * @speed, 0)

  stop: (dir) ->
    if dir? and dir isnt @dir
      return

    if not dir? and @dir is 0
      return

    if not dir?
      dir = @dir

    @parent.movement.move(-1 * dir * @speed, 0)
    @dir = 0
    @trigger('stop', dir)

  isWalking: ->
    @dir isnt 0

module.exports = Walking
