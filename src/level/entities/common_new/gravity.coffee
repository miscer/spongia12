bond = require 'bond'

class Gravity extends bond.components.Component
  constructor: (@a) ->

  attached: ->
    movement = @parent.getComponent('movement')

    movement.on 'down', ->
      if movement.velocity.y > 0
        movement.velocity.y = 0

  update: (dt) ->
    if @parent.movement.velocity.y > 300
      return
    
    @parent.movement.move(0, @a * dt)

module.exports = Gravity
