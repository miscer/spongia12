love = require 'lovejs'
bond = require 'bond'

class DamageNotifier extends bond.components.Component
  VISIBLE = 0.5

  constructor: (@color) ->
    @visible = false
    @timer = 0
    @dimage = null

  attached: ->
    fighting = @parent.getComponent('fighting')
    fighting.on('hurt', @hurt)

  update: (dt) ->
    if not @visible
      return

    @timer += dt

    if @timer > VISIBLE
      @visible = false

  draw: ->
    if not @visible
      return

    pos = @parent.movement.getPosition()
    w = @parent.movement.getAABB().getWidth()

    love.graphics.push()
    love.graphics.setFont('Pixel', 15)
    love.graphics.setColor(@color[0], @color[1], @color[2])
    love.graphics.setTextAlign('center')
    love.graphics.print("-#{@damage}", pos.x + (w / 2), pos.y - 50)
    love.graphics.pop()

  hurt: (damage) =>
    @visible = true
    @timer = 0
    @damage = damage

module.exports = DamageNotifier
