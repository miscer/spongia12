love = require 'lovejs'
bond = require 'bond'

class Levels extends bond.components.Component
  constructor: ->
    @main = 1
    @points = 0

    @attack =
      value: 1
      max: 7

    @vitality =
      value: 1
      max: 7

    @robin =
      value: 1
      max: 7

    @speed =
      value: 1
      max: 5

    @luck =
      value: 1
      max: 5

    love.eventify(this)

  get: (name) ->
    if not @[name]
      throw new Error("there is no level called #{name}")

    @[name].value

  max: (name) ->
    @[name].max

  set: (name, value) ->
    if value > @max(name)
      throw new Error("value #{value} is higher than maximum for #{name} level")

    @[name].value = value
    @trigger('update')

  up: (name) ->
    @set(name, @get(name) + 1)

  mainUp: ->
    @main++

    @points += Math.round((@main + 3) * 1.2)

    @trigger('update')
    @trigger('mainUp')

  getCost: (name) ->
    @get(name) + 1

  isMax: (name) ->
    @get(name) >= @max(name)

module.exports = Levels
