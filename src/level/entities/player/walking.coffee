Walking = require '../common_new/walking'

class PlayerWalking extends Walking
  constructor: ->
    super

    @blocked = false

  attached: ->
    dialog = @parent.parent.dialog
    
    dialog.on('start', @block)
    dialog.on('end', @unblock)

  block: =>
    @blocked = true
    @stop()

  unblock: =>
    @blocked = false

  start: ->
    if not @blocked
      super


module.exports = PlayerWalking
