Fighting = require '../common_new/fighting'

class PlayerFighting extends Fighting
  attack: (enemy, type) ->
    attack =
      switch type
        when 'normal' then @parent.mechanics.getAttack()
        when 'special' then @parent.mechanics.getSpecialAttack()

    super(enemy, attack)

    if not enemy._playered
      enemy._playered = true
      enemy.points.on('dead', @parent.points.incExp)

module.exports = PlayerFighting
