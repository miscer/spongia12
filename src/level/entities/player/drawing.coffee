love = require 'lovejs'
{animation, components} = require 'bond'

class Drawing extends components.Component
  constructor: ->
    @asset = love.assets.addImage('images/player.png')

  init: ->
    image = love.graphics.newImage(@asset)

    @reel = new animation.Reel(image, 80, 160)
    @anims =
      standing:
        front: new animation.Nonlinear(@reel, [[1, 0.7], [2, 0.2], [1, 3], [2, 0.2]])
        left: new animation.Nonlinear(@reel, [[3, 0.7], [4, 0.2], [3, 3], [4, 0.2]])
        right: new animation.Nonlinear(@reel, [[5, 0.7], [6, 0.2], [5, 3], [6, 0.2]])
      walking:
        left: new animation.Animation(@reel, 0.1, [28, 29, 30, 31, 32, 33, 34, 35, 36, 31, 32, 33])
        right: new animation.Animation(@reel, 0.1, [19, 20, 21, 22, 23, 24, 25, 26, 27, 22, 23, 24])
    
    @stand('front')

    @parent.walking.on('start', @wstart)
    @parent.walking.on('stop', @wstop)

  walk: (dir) ->
    @anims.active = @anims.walking[dir]

  stand: (dir) ->
    @anims.active = @anims.standing[dir]

  wstart: (dir) =>
    if dir is 1
      @dir = 'right'
    else if dir is -1
      @dir = 'left'

    @walk(@dir)

  wstop: =>
    @stand('front')

  update: (dt) ->
    @anims.active.update(dt)

  draw: ->
    pos = @parent.getPosition()
    x = ((pos.x + 0.5) | 0) - 17
    y = ((pos.y + 0.5) | 0) - 6
    love.graphics.draw(@anims.active, x, y)

module.exports = Drawing
