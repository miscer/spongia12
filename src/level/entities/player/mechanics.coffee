bond = require 'bond'

Mechanics = require '../common_new/mechanics'

class PlayerMechanics extends Mechanics
  attached: ->
    @levels = @parent.getComponent('levels')

  getMaxHP: ->
    Math.round(75 * (1 + ((@levels.get('vitality') + 4) / 10)) + Math.pow(1.6, @levels.get('vitality') + 4))

  getMaxSP: ->
    Math.round(30 + Math.pow(1.5, 3 + @levels.get('robin')))

  getMaxXP: ->
    Math.round(Math.pow(1.2, 9 + @levels.main))

  getDefense: ->
    Math.round((@levels.get('vitality') * (@levels.get('vitality') + 1)) / 2)

  getAttack: ->
    Math.round(5 + Math.pow(1.45, @levels.get('attack') + 4))

  getSpecialAttack: ->
    Math.round(5 + Math.pow(1.45, @levels.get('robin') + 7))

  getAttackSpeed: ->
    0.5 - (0.1 * @levels.get('speed'))

  getHpotStrength: ->
    Math.round(Math.pow(1.6, 4 + @levels.get('vitality')))

  getSpotStrength: ->
    Math.round(10 + Math.pow(1.3, 2 + @levels.get('robin')))

  getSpecialAttackCost: ->
    15

module.exports = PlayerMechanics
