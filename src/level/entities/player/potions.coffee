love = require 'lovejs'
bond = require 'bond'

class Potions extends bond.components.Component
  constructor: ->
    @health = 0
    @special = 0

    love.eventify(this)

  addHealth: ->
    @health++
    @trigger('update')

  addSpecial: ->
    @special++
    @trigger('update')

  useHealth: =>
    if @health is 0
      return

    add = @parent.mechanics.getHpotStrength()
    @parent.points.add('health', add)

    @health--
    @trigger('update')

  useSpecial: =>
    if @special is 0
      return

    add = @parent.mechanics.getSpotStrength()
    @parent.points.add('special', add)

    @special--
    @trigger('update')

module.exports = Potions
