bond = require 'bond'

class Jumping extends bond.components.Component
  constructor: (@strength) ->
    @jumping = false
    @double = false

  attached: ->
    movement = @parent.getComponent('movement')

    movement.on 'up', =>
      if @jumping
        movement.velocity.y = 0

    movement.on 'down', =>
      @jumping = false
      @double = false

  jump: ->
    if not @jumping
      @jumping = true
      @parent.movement.move(0, -1 * @strength)
    
    else if not @double and @parent.movement.velocity.y < 0
      @double = true
      @parent.movement.move(0, -0.8 * @strength)

module.exports = Jumping
