{components} = require 'bond'
Gravity = require '../common_new/gravity'
DamageNotifier = require '../common_new/damage_notifier'
Walking = require './walking'
Jumping = require './jumping'
Movement = require './movement'
KeyboardControls = require './keyboard_controls'
Drawing = require './drawing'
Points = require './points'
Levels = require './levels'
Mechanics = require './mechanics'
Fighting = require './fighting'
Potions = require './potions'

class Player extends components.Entity
  constructor: ->
    @robin = null

  createComponentWalking: ->
    new Walking(200)

  createComponentJumping: ->
    new Jumping(450)

  createComponentGravity: ->
    new Gravity(1000)

  createComponentKeyboard: ->
    new KeyboardControls

  createComponentDrawing: ->
    new Drawing

  createComponentPoints: ->
    new Points

  createComponentLevels: ->
    new Levels

  createComponentMovement: ->
    collisions = @parent.getComponent('collisions')

    movement = new Movement(collisions, 'player')
    movement.setDimensions(46, 154)
    movement

  createComponentMechanics: ->
    new Mechanics

  createComponentFighting: ->
    new Fighting

  createComponentDamageNotifier: ->
    new DamageNotifier([0xff, 0x24, 0x24])

  createComponentPotions: ->
    new Potions

  update: (dt) ->
    @gravity.update(dt)
    @movement.update(dt)
    @fighting.update(dt)
    @drawing.update(dt)
    @damageNotifier.update(dt)

  # getters

  getPosition: ->
    @movement.getPosition()

  setPosition: (x, y) ->
    @movement.setPosition(x, y)

  getAABB: ->
    @movement.getAABB()

  getRobin: ->
    @robin

  setRobin: (robin) ->
    robin.setPlayer(this)
    @robin = robin

  # actions

  normalAttack: =>
    if @robin?
      @robin.attacking.attack()

  specialAttack: =>
    if @robin?
      @robin.attacking.special()

module.exports = Player
