Movement = require '../common_new/movement'

class PlayerMovement extends Movement
  getBehindHead: ->
    @behindHead ?= {}

    pos = @getPosition()
    w = @getAABB().getWidth()
    dir = if @getDir() is 'left' then 1 else -1

    @behindHead.x = pos.x + dir * (w / 2 + 50)
    @behindHead.y = pos.y + 20

    @behindHead

module.exports = PlayerMovement
