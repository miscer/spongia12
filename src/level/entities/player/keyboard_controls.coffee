love = require 'lovejs'
{components} = require 'bond'

class KeyboardControls extends components.Component
  constructor: ->
    @allowed =
      walking: true
      jumping: true

    love.keyboard.on('pressed', @pressed)
    love.keyboard.on('released', @released)

  pressed: (key) =>
    switch key
      when 'up'
        @parent.jumping.jump() if @allowed.jumping
      when 'left'
        @parent.walking.start(-1) if @allowed.walking
      when 'right'
        @parent.walking.start(1) if @allowed.walking
      when 'q'
        @parent.normalAttack()
      when 'e'
        @parent.specialAttack()
      when 'x'
        @parent.potions.useHealth()
      when 'c'
        @parent.potions.useSpecial()

  released: (key) =>
    switch key
      when 'left'
        @parent.walking.stop(-1)
      when 'right'
        @parent.walking.stop(1)

  allow: (what) ->
    @allowed[what] = true

  disallow: (what) ->
    @allowed[what] = false

module.exports = KeyboardControls
