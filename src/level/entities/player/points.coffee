love = require 'lovejs'
bond = require 'bond'

class Points extends bond.components.Component
  constructor: ->
    @health =
      points: 30
      max: 190

    @special =
      points: 17
      max: 90

    @experience =
      points: 87
      max: 100

    love.eventify(this)

  attached: ->
    fighting = @parent.getComponent('fighting')
    fighting.on('hurt', @hurt)

    levels = @parent.getComponent('levels')
    levels.on('update', @lvlUpdate)

    @mechanics = @parent.getComponent('mechanics')
    
    @lvlUpdate()

  # --- manipulation ---

  get: (name) ->
    @[name].points

  set: (name, value) ->
    @[name].points = value
    @trigger('update')
    value

  max: (name) ->
    @[name].max

  add: (name, n) ->
    @set(name, @get(name) + n)

    if @get(name) > @max(name)
      @set(name, @max(name))

  subtract: (name, n) ->
    @set(name, @get(name) - n)

    if @get(name) < 0
      @set(name, 0)

  # --- actions ---

  hurt: (damage) =>
    @subtract('health', damage)

    if @get('health') is 0
      @trigger('dead')

  incExp: =>
    @add('experience', 1)

    if @get('experience') is @max('experience')
      @set('experience', 0)
      @parent.levels.mainUp()

  lvlUpdate: =>
    @health.max = @mechanics.getMaxHP()
    @set('health', @health.max)

    @special.max = @mechanics.getMaxSP()
    @set('special', @special.max)

    @experience.max = @mechanics.getMaxXP()
    @set('experience', 0)

  # ---

  canDoSpecialAttack: ->
    @get('special') > @mechanics.getSpecialAttackCost()

module.exports = Points
