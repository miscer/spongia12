bond = require 'bond'

class Dying extends bond.components.Component
  attached: ->
    points = @parent.getComponent('points')
    points.on('dead', @dead)

  dead: =>
    @died = true

    @parent.parent.trigger('dead', @parent)

    @parent.walking.stop()
    @parent.movement.stop()
    @parent.removeComponent('attacking')
    @parent.removeComponent('chasing')

    @uncollide()

    anim = @parent.drawing.die(@parent.movement.getDir())
    anim.end = @vanish

  update: ->
    if @died
      @parent.drawing.die(@parent.movement.getDir())

  uncollide: ->
    enemies = @parent.parent
    collider = @parent.movement.collider
    enemies.getLevel().collisions.remove(collider)

  vanish: =>
    enemies = @parent.parent
    enemies.remove(@parent)

module.exports = Dying
