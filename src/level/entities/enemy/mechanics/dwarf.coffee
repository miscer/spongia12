Mechanics = require '../../common_new/mechanics'

class DwarfMechanics extends Mechanics
  getAttack: ->
    32

  getMaxHP: ->
    622

  getAttackSpeed: ->
    1.2

  getDefense: ->
    23

module.exports = DwarfMechanics
