bond = require 'bond'

Mechanics = require '../../common_new/mechanics'

class CarrotMechanics extends Mechanics
  constructor: ->
    @stage = null

  getStage: ->
    if not @stage?
      throw new Error("no stage set")
    @stage

  setStage: (stage) ->
    @stage = stage

  getAttack: ->
    Math.round(2 + Math.pow(1.35, @getStage() + 3))

  getMaxHP: ->
    Math.round(25 * (@getStage() + 3) + Math.pow(1.55, @getStage() + 2))

  getAttackSpeed: ->
    0.6

  getDefense: ->
    Math.round(2 + Math.pow(1.35, @getStage() + 3))

module.exports = CarrotMechanics
