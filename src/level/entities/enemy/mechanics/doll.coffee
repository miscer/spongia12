Mechanics = require '../../common_new/mechanics'

class DollMechanics extends Mechanics
  getAttack: ->
    70

  getMaxHP: ->
    922

  getAttackSpeed: ->
    0.5

  getDefense: ->
    30

module.exports = DollMechanics
