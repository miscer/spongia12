bond = require 'bond'

Mechanics = require '../../common_new/mechanics'

class TomatoMechanics extends Mechanics
  constructor: ->
    @stage = null

  getStage: ->
    if not @stage?
      throw new Error("no stage set")
    @stage

  setStage: (stage) ->
    @stage = stage

  getAttack: ->
    Math.round(2 + Math.pow(1.4, @getStage() + 3))

  getMaxHP: ->
    Math.round(30 * (1 + ((@getStage() + 2) / 10)) + Math.pow(1.6, @getStage() + 2))

  getAttackSpeed: ->
    0.9

  getDefense: ->
    Math.round(2 + Math.pow(1.35, @getStage() + 2))

module.exports = TomatoMechanics
