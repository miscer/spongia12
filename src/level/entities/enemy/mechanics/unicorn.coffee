Mechanics = require '../../common_new/mechanics'

class UnicornMechanics extends Mechanics
  getAttack: ->
    28

  getMaxHP: ->
    272

  getAttackSpeed: ->
    0.8

  getDefense: ->
    11

module.exports = UnicornMechanics
