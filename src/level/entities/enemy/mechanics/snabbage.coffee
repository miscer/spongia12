bond = require 'bond'

Mechanics = require '../../common_new/mechanics'

class SnabbageMechanics extends Mechanics
  constructor: ->
    @stage = null

  getStage: ->
    if not @stage?
      throw new Error("no stage set")
    @stage

  setStage: (stage) ->
    @stage = stage

  getAttack: ->
    Math.round(2 + Math.pow(1.35, @getStage() + 2))

  getMaxHP: ->
    Math.round(40 * (@getStage() + 3) + Math.pow(1.7, @getStage() + 2))

  getAttackSpeed: ->
    1.2

  getDefense: ->
    Math.round(2 + Math.pow(1.4, 3 + @getStage()))

module.exports = SnabbageMechanics
