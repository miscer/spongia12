Mechanics = require '../../common_new/mechanics'

class PenguinMechanics extends Mechanics
  getAttack: ->
    80

  getMaxHP: ->
    1200

  getAttackSpeed: ->
    1.2

  getDefense: ->
    50

module.exports = PenguinMechanics
