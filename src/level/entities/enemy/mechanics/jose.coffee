Mechanics = require '../../common_new/mechanics'

class JoseMechanics extends Mechanics
  getAttack: ->
    14

  getMaxHP: ->
    140

  getAttackSpeed: ->
    1

  getDefense: ->
    5

module.exports = JoseMechanics
