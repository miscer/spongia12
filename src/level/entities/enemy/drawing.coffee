love = require 'lovejs'
bond = require 'bond'

class Drawing extends bond.components.Component
  setReel: (reel) ->
    @reel = reel

  attached: ->
    @parent.getComponent('walking').on('start', @wstart)
    @parent.getComponent('chasing').on('caught', @astart)
    @parent.getComponent('fighting').on('hurt', @hurt)

  init: ->
    if not @reel?
      throw new Error("No reel set")
    
    @animations =
      walking:
        left: new bond.animation.Animation(@reel, 0.5, [1, 2])
        right: new bond.animation.Animation(@reel, 0.5, [20, 19])
      attacking:
        left: new bond.animation.Animation(@reel, @getAttackInterval(), [5, 6])
        right: new bond.animation.Animation(@reel, @getAttackInterval(), [24, 23])
      defense:
        left: new bond.animation.Animation(@reel, 0.2, [9])
        right: new bond.animation.Animation(@reel, 0.2, [28])
      dying:
        left: new bond.animation.Animation(@reel, 0.5, [13, 14, 15, 16])
        right: new bond.animation.Animation(@reel, 0.5, [32, 31, 30, 29])

    @active = @animations.walking.right

  getAttackInterval: ->
    switch @parent.name
      when 'tomato' then 0.45
      when 'carrot' then 0.3
      when 'snabbage' then 0.6
      when 'doll', 'dwarf', 'jose', 'penguin', 'unicorn'
        0.5

  walk: (dir) ->
    @active = @animations.walking[dir]

  attack: (dir) ->
    @active = @animations.attacking[dir]

  defend: (dir) ->
    @active = @animations.defense[dir]

  die: (dir) ->
    @active = @animations.dying[dir]

  astart: =>
    @attack(@parent.movement.getDir())

  wstart: (dir) =>
    if dir is 1
      @dir = 'right'
    else if dir is -1
      @dir = 'left'

    @walk(@dir)

  hurt: =>
    @defend(@parent.movement.getDir()).end = @astart

  animend: ->
    @walk(@dir)

  update: (dt) ->
    @active.update(dt)

  draw: ->
    pos = @parent.getPosition()
    love.graphics.draw(@active, pos.x, pos.y)

module.exports = Drawing
