love = require 'lovejs'
bond = require 'bond'

class HealthBar extends bond.components.Component
  WIDTH = 100
  HEIGHT = 6

  constructor: ->
    @visible = false

  attached: ->
    points = @parent.getComponent('points')
    points.on('update', @redraw)

  init: ->
    @canvas = love.graphics.newCanvas(100, 6)

  draw: ->
    pos = @parent.getPosition()
    pw = @parent.getAABB().getWidth()
    x = pos.x + (pw - WIDTH) / 2
    y = pos.y - 20

    love.graphics.draw(@canvas, x, y)

  redraw: =>
    health = @parent.points.health.value
    max = @parent.points.health.max

    love.graphics.setCanvas(@canvas)

    love.graphics.setColor(0xff, 0xff, 0xff)
    love.graphics.rectangle('fill', 0, 0, WIDTH, HEIGHT)

    width = Math.round(WIDTH * (health / max))
    love.graphics.setColor(0xd4, 0x68, 0x51)
    love.graphics.rectangle('fill', 0, 0, width, HEIGHT)

    love.graphics.setColor(0, 0, 0)
    love.graphics.rectangle('line', 0, 0, WIDTH, HEIGHT)

    love.graphics.setCanvas()

module.exports = HealthBar
