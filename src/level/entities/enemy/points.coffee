love = require 'lovejs'
bond = require 'bond'

class Points extends bond.components.Component
  constructor: ->
    @health =
      value: null
      max: null

    love.eventify(this)

  attached: ->
    fighting = @parent.getComponent('fighting')
    fighting.on('hurt', @hurt)

  init: ->
    mechanics = @parent.getComponent('mechanics')
    @health.value = @health.max = mechanics.getMaxHP()

  hurt: (damage) =>
    @health.value -= damage
    @trigger('update')
    
    if @health.value <= 0
      @trigger('dead')

module.exports = Points
