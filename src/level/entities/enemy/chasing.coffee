love = require 'lovejs'
bond = require 'bond'

class Chasing extends bond.components.Component
  constructor: ->
    @chasing = null
    @distance = new bond.vectors.Vector

    love.eventify(this)

  chase: (entity, min = 100) ->
    its = entity.getPosition()
    mine = @parent.getPosition()

    dist = its.x - mine.x
    abs = Math.abs(dist)

    if abs < min
      @trigger('caught', entity)
    else
      dir = dist / abs
      @parent.walking.start(dir)

  update: ->
    @parent.walking.stop()

module.exports = Chasing
