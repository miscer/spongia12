love = require 'lovejs'
bond = require 'bond'

class Attacking extends bond.components.Component
  constructor: ->
    @isHurt = false
    @distance = new bond.vectors.Vector

    love.eventify(this)

  attached: ->
    fighting = @parent.getComponent('fighting')
    fighting.on('hurt', @hurt)

    chasing = @parent.getComponent('chasing')
    chasing.on('caught', @caught)

  hurt: (damage, @player) =>
    @isHurt = true

  caught: (@player) =>
    f = @parent.fighting
    dist = @getDistance()

    if f.canAttack() and dist.length2() < 14400
      f.attack(@player)

  getDistance: ->
    mine = @parent.getPosition()
    players = @player.getPosition()

    @distance.vupdate(players).vsubtract(mine)
    @distance

  update: ->
    if @isHurt
      @parent.chasing.chase(@player)

module.exports = Attacking

###

  chase: (@player) ->
    if @state is 'idle'
      @state = 'aggresive'

  stopChasing: ->
    if @player? and @state is 'aggresive'
      @state = 'idle'

  update: ->
    if not @player?
      return

    @updateDistance()

    if @state in ['aggresive', 'hurt']
      @follow()
      @attack()

    if @state is 'idle'
      @parent.walking.stop()

    @stopChasing()

  updateDistance: ->
    mine = @parent.movement.getPosition()
    players = @player.movement.getPosition()

    @distance.vupdate(players).vsubtract(mine)

  follow: ->
    abs = Math.abs(@distance.x)

    if abs > 100
      @parent.walking.start(@distance.x / abs)
    else
      if @parent.walking.isWalking()
        @parent.walking.stop()
        @trigger('approached')

  attack: ->
    if @distance.length2() <= 14400 and @parent.fighting.canAttack()
      @parent.fighting.attack(@player)

module.exports = Attacking
