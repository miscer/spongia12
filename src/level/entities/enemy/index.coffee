bond = require 'bond'

Movement = require '../common_new/movement'
Walking = require '../common_new/walking'
Gravity = require '../common_new/gravity'
Fighting = require '../common_new/fighting'
DamageNotifier = require '../common_new/damage_notifier'
Drawing = require './drawing'
Points = require './points'
Dying = require './dying'
HealthBar = require './health_bar'
Attacking = require './attacking'
Chasing = require './chasing'

TomatoMechs = require './mechanics/tomato'
CarrotMechs = require './mechanics/carrot'
SnabbageMechs = require './mechanics/snabbage'
DollMechs = require './mechanics/doll'
DwarfMechs = require './mechanics/dwarf'
JoseMechs = require './mechanics/jose'
PenguinMechs = require './mechanics/penguin'
UnicornMechs = require './mechanics/unicorn'

class Enemy extends bond.components.Entity
  constructor: (@name) ->

  createComponentGravity: ->
    new Gravity(1000)

  createComponentMovement: ->
    collisions = @parent.getLevel().collisions
    new Movement(collisions, 'enemy')

  createComponentWalking: ->
    new Walking(100)

  createComponentDrawing: ->
    new Drawing

  createComponentMechanics: ->
    switch @name
      when 'tomato' then new TomatoMechs
      when 'carrot' then new CarrotMechs
      when 'snabbage' then new SnabbageMechs
      when 'doll' then new DollMechs
      when 'dwarf' then new DwarfMechs
      when 'jose' then new JoseMechs
      when 'penguin' then new PenguinMechs
      when 'unicorn' then new UnicornMechs

  createComponentFighting: ->
    new Fighting(@getComponent('mechanics'))

  createComponentPoints: ->
    new Points

  createComponentDying: ->
    new Dying

  createComponentDamageNotifier: ->
    new DamageNotifier([0xff, 0xff, 0xff])

  createComponentHealthBar: ->
    new HealthBar

  createComponentAttacking: ->
    new Attacking

  createComponentChasing: ->
    new Chasing

  getPosition: ->
    @movement.getPosition()

  setPosition: (x, y) ->
    @movement.setPosition(x, y)

  getAABB: ->
    @movement.getAABB()

  setDimensions: (w, h) ->
    @movement.setDimensions(w, h)

module.exports = Enemy
