level = null
dialogbox = null
robin = null

dialog_go = [
  ['robin-black', 'A sme tu.']
  ['you', 'Kde tu?']
  ['robin-black', 'Na konci. Vidíš toho tučniaka?']
  ['you', 'On mi bude vedieť povedať, kde je posledná príšera, cez ktorú musím prejsť?']
  ['robin-black', '...']
  ['robin-black', 'Ty si ho naozaj nepamätáš.']
  ['robin-black', 'Nie, chmuľo. On je tá "posledná príšera".']
  ['robin-black', 'Kebyže nemusím bojovať, tak by som odišiel. Nechcem s ním mať nič spoločné.']
  ['you', 'Veď je to len malý plyšový tučniak!']
  ['robin-black', 'Ako myslíš.']
]

talked = false
talk_go = ->
  if not talked
    talked = true
    for [author, msg] in dialog_go
      dialogbox.print(author, msg)

  level.enemies.getBoss().points.once('dead', fly_away)

fly_away = ->
  robin.following.stop()

  dest = exports.away
  robin.flying.flyTo(dest.x, dest.y)

  robin.flying.once('end', talk_storyteller)

dialog_storyteller = [
  ['storyteller', 'Červienky ťa opustili, ale ja som sa vrátil. Svoj sľub dodržali, pomohli ti dostať sa cez všetky príšery. Svoju pamäť si ale nikde nenašiel.']
  ['you', 'Rád ťa znovu počujem.']
  ['storyteller', 'Tomu sa ťažko verí, po tom čo si ma tak ľahko vymenil.']
  ['you', 'Veď ty vieš, že som musel!']
  ['storyteller', 'Chápem ťa. Odkopli ma aj lepší ako ty. V tejto dobe rozprávača potrebuje len málo kto.']
  ['storyteller', 'Moja práca ale je pomôcť ti. Preto ti radím: mal by si prejsť k tučniakovi.']
  ['storyteller', 'Mimochodom, dúfam, že máš výčitky svedomia.']
  ['storyteller', 'Všimol si si, že srdiečko na jeho bruchu je v skutočnosti kapsička!']
  ['storyteller', 'Vytŕča z nej kus papieru.']
  ['storyteller', 'Vytiahneš ho. Vyzerá ako obyčajná strana vytrhnutá z diára. V hlave sa ti vynoria všetky rady, ktoré ti kedy červienky dávali.']
  ['storyteller', 'Hlavne štvrtá.']
  ['storyteller', 'Je dôležité to, čo urobíš po tom, čo odpovede a otázky dostaneš.']
  ['storyteller', 'Keď zistíš otázku a nájdeš odpoveď, budeš sa musieť rozhodnúť.']
  ['storyteller', 'Zrazu máš strach, bojíš sa, čo zistíš. Bojíš sa, že od teba ľudia očakávať <i>veci</i>.']
  ['storyteller', 'Nič nie je horšie, ako keď od teba ľudia očakávajú <i>veci</i>.']
  ['storyteller', 'Veľkú pomstu, alebo dačo. Možno sa budeš musieť stať samozvaným strážcom zákona a o červienky sa navždy starať.']
  ['storyteller', 'Ty nie si hrdina. Si len chlapec, ktorý zabudol, kto je.']
  ['storyteller', 'A určite si to zabudol z nejakého dôvodu. Voči tomu dôvodu by nebolo fér odhaliť ho. Dôvody sa skrývajú iba ak na to sami majú dôvod.']
  ['storyteller', 'Preto listy pustíš na zem a otočíš sa.']
  ['storyteller', 'Pravdepodobne ideš nájsť najbližšiu autobusovú zastávku, ktorá je dva kilometre južne. Pri kukuričnom poli, nemôžeš ju minúť.']
]

game = require '../../../game'

talk_storyteller = ->
  for [author, msg] in dialog_storyteller
    dialogbox.print(author, msg)

  dialogbox.once 'end', ->
    game.credits()

exports.trigger = ->
  level = this
  dialogbox = @dialog
  robin = @robins.get('black')

  talk_go()

exports.away = null # set from map
  
