dialog = [
  ['robin-black', 'Neverím, konečne sa ti uráčilo dostaviť sa.']
  ['you', 'Tvoja sestra spomínala, že nerád čakáš.']
  ['robin-black', 'Nerád čakám a nerád pomáham ľuďom, ako ty.']
  ['robin-black', 'Ale aj tak nemám na výber, čiže poď, nechcem s tebou tráviť viac času než je treba.']
  ['you', 'Prečo? Urobil som ti snáď niečo?']
  ['robin-black', 'Prečo? Myslíš si, že áno?']
  ['you', 'Stratil som predsa pamäť, to musíš vedieť. Neviem, čo sa dialo predtým, než som sa tu ukázal.']
  ['robin-black', 'Tak mi bolo povedané.']
  ['you', 'Čiže?']
  ['robin-black', 'Čiže si pohni, nech to máme všetko z krku. Sme skoro na úplnom konci.']
]

talked = false

exports.trigger = ->
  if not talked
    talked = true
    for [author, msg] in dialog
      @dialog.print(author, msg)
