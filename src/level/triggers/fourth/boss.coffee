level = null
dialogbox = null
cardbox = null
robin = null

dialog_go = [
  ['you', 'To je človek?']
  ['robin-pink', 'Ale houby, len bábika. A pozri sa v akom stave. Až by mi bolo ľúto na ňu útočiť.']
  ['you', 'Čiže sa jej mám len vyhnúť?']
  ['robin-pink', 'Povedala by som, že by mi to bolo ľúto, nie že mi je. Čiže zavri otvor a poďme do nej, tiger!']
]

talked = false
talk_go = ->
  if not talked
    talked = true
    for [author, msg] in dialog_go
      dialogbox.print(author, msg)

  cardbox.once('hide', talk_bye)

dialog_bye = [
  ['robin-pink', 'Videl si to! Videl si, ako som jej dala posledný úder! To bolo skvelé!']
  ['robin-pink', 'Až by som s tebou rada ostala, aby sme si to zopakovali, ale už na teba čaká môj brat. Veľmi nerád čaká, poviem ti to tak.']
  ['you', 'Si si istá, že so mnou nemôžeš ostať aj ďalej?']
  ['you', 'Bola si asi najpríjemnejšia spoločnosť.']
  ['robin-pink', 'Ah, krpec, budem sa červenať! Rada to počujem, ale nie.']
  ['robin-pink', 'Pred tým než odídem, skús si zapamätať, že nie sú až tak dôležité odpovede a otázky, ale skôr to, čo urobíš po tom, ako ich dostaneš.']
]

talk_bye = ->
  for [author, msg] in dialog_bye
    dialogbox.print(author, msg)

  dialogbox.once('end', fly_away)

fly_away = ->
  robin.following.stop()

  dest = exports.away
  robin.flying.flyTo(dest.x, dest.y)

  robin.flying.once('end', next_stage)

next_stage = ->
  level.next()

exports.trigger = ->
  level = this
  dialogbox = @dialog
  cardbox = @cardbox
  robin = @robins.get('pink')

  talk_go()

exports.away = null # set from map
  
