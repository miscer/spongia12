dialog = [
  ['robin-pink', 'Hej! Už si skoro na konci!']
  ['you', 'Naozaj? Koľko ešte?']
  ['robin-pink', 'To ti nemôžem povedať. Čo by to bolo za život, keby sme vedeli, kam ideme?']
  ['you', 'Prosím, nezačni znieť ako tvoj brat, buď aspoň ty zrozumiteľný.']
  ['robin-pink', 'Zrorumiteľná! Som jediné dievča v našej rodine, mladý pán. Čo to nie je vidieť?']
  ['you', 'Prepáč, nemyslel som to--']
  ['robin-pink', 'Len žartujem! Aj to človek raz za čas musí skúsiť, nemám pravdu? Hlavne tu.']
  ['you', 'Pozeráš sa na to inak, než všetci, ktorých som doteraz stretol.']
  ['robin-pink', 'Mám to v popise práce, krpec.']
  ['robin-pink', 'Ako si naznačil sám, všetci sú tu iní. Preto tu musí byť aspoň jedna červienka, ktorá sa bude správať, akoby sa jej to netýkalo.']
  ['robin-pink', 'Občas uvažujem, či ten, ktorý to tu vytvoril zamýšľal aj to. Vieš, či bolo to, ako skončíme súčasťou experimentu.']
  ['robin-pink', 'Pri šialených vedcoch nikdy nevieš.']
  ['you', 'Už nie si zrozumiteľná ani ty.']
  ['robin-pink', 'Ver mi, že to pochopíš. Hlavne, ak sa nám podarí dostať až do cieľa.']
]

talked = false

exports.trigger = ->
  if not talked
    talked = true
    for [author, msg] in dialog
      @dialog.print(author, msg)
