level = null
dialogbox = null
cardbox = null
robin = null

dialog_go = [
  ['you', 'Čo to je?!']
  ['robin-red', 'Bol to jednorožec. Už nie je. Bože, pozri sa na tie jeho veľké roztrasené oči.']
  ['robin-red', 'Už chápeš čo som myslel tým, že to miesto s tebou urobí veci?']
  ['you', 'Asi áno.']
  ['robin-red', 'Super, tak za kávovú lyžičku rozumu máš. Asi som ti krivdil.']
  ['robin-red', 'Ale rýchlo, doňho. Chcem byť doma pred piatou, nedal som si nahrať seriál.']
]

talked = false
talk_go = ->
  if not talked
    talked = true
    for [author, msg] in dialog_go
      dialogbox.print(author, msg)

  cardbox.once('hide', talk_bye)

dialog_bye = [
  ['robin-red', 'Hm. Spolupracovalo sa mi s tebou lepšie, než som očakával.']
  ['you', 'To znamená, že ma neopustíš?']
  ['robin-red', 'Nelichoť si, mladý, s tým seriálom som to myslel vážne.']
  ['robin-red', 'Ale veľa šťastia.']
]

talk_bye = ->
  for [author, msg] in dialog_bye
    dialogbox.print(author, msg)

  dialogbox.once('end', fly_away)

fly_away = ->
  robin.following.stop()

  dest = exports.away
  robin.flying.flyTo(dest.x, dest.y)

  robin.flying.once('end', next_stage)

next_stage = ->
  level.next()

exports.trigger = ->
  level = this
  dialogbox = @dialog
  cardbox = @cardbox
  robin = @robins.get('red')

  talk_go()

exports.away = null # set from map
  
