dialog = [
  ['robin-red', 'To to trvalo. Vitaj a pohni si, nemôžeme strácať celý deň.']
  ['you', 'Počkaj - nič mi o tom, čo ma čaká, nepovieš?']
  ['robin-red', 'Čo by som ti mal hovoriť? Všetko časom zistíš sám, teraz už choď, Zelenina sa nevykynoží sama. Musíme jej pomôcť.']
  ['you', 'Páni, ty si naozaj ako veľké klbko šťastia a prívetivosti.']
  ['robin-red', 'Čo by si odomňa čakal? Byť v tomto zapadákove sa na tebe odrazí.']
  ['robin-red', 'Keby že sa tu za rovnakých okolností ukážeš pred tromi rokmi, možno by som ti dokonca daroval môj vtáči zob.']
  ['you', 'Ja predsa nejem vtáči zob.']
  ['robin-red', '...']
  ['robin-red', 'Ja viem, ty truľo. Bola to metafora. Také to, keď povieš niečo, čo nemyslíš doslova, len na prirovnanie.']
  ['you', 'Viem, čo je to metafora!']
  ['robin-red', 'Naozaj? A odkiaľ? Veď nemáš viac ako osem rokov.']
  ['you', 'To neviem, ale viem, že to viem. Možno som sa to učil v škole.']
  ['robin-red', 'Hm. Ako myslíš. No teraz pohyb. Nezabíjajme čas, ale Zeleninu.']
]

talked = false

exports.trigger = ->
  if not talked
    talked = true
    for [author, msg] in dialog
      @dialog.print(author, msg)
