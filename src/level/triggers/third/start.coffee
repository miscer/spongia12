dialog = [
  ['robin-yellow', 'Vitaj v ďalšom leveli! Teraz ťa budem sprevádzať ja.']
  ['you', 'Vďaka bohu, si milší ako ten predtým.']
  ['robin-yellow', 'Mal si česť stretnúť môjho brata, predpokladám. Úprimnú sústrasť.']
  ['you', 'Ďakujem, myslím, že si sústrasť zaslúžim.']
  ['robin-yellow', 'Keď si na neho zvykneš, nie je to tak zlé! Ani on sám nie je tak zlý.']
  ['robin-yellow', 'Ide hlavne o to, že musíš pochopiť, aké pre nás je tu vyrastať.']
  ['you', 'Už sa mi to snažil vysvetliť. Myslím, že to chápem.']
  ['robin-yellow', 'Dovolím si pochybovať. Spoločnosť, ktorá vznikla v troskách tejto dediny, je príliš zložitá a mnohotvárna na to, aby ti ju môj brat dokázal len načrtnúť.']
  ['you', 'Znie to, akoby si o ňom mal teda vysokú mienku. Prípadne o mne.']
  ['robin-yellow', 'Asi oboje.']
  ['you', 'Ďakujem.']
  ['robin-yellow', 'Niet za čo.']
  ['robin-yellow', 'Tak či tak, skončil som pri komplexnosti našej spoločnosti.']
  ['robin-yellow', 'Ani mne sa nepodarí ti to vysvetliť za pár minút, ani za pár hodín, ale musíš pochopiť, že keď hovorím o vzniku novej spoločnosti, myslím to doslovne. Normy boli zbúrané, ideály zase vystavané, pričom ale väčšia toho, čo tu vidíš, sú len tiene minulosti.']
  ['you', 'Myslím, že som zmätený.']
  ['robin-yellow', 'To väčšina tvorov, s ktorými sa rozprávam. Ale ty by si ani nemusel.']
  ['robin-yellow', 'No nezaháľaj, mali by sme ísť ďalej.']
]

talked = false

exports.trigger = ->
  if not talked
    talked = true
    for [author, msg] in dialog
      @dialog.print(author, msg)
