level = null
dialogbox = null
cardbox = null
robin = null

dialog_go = [
  ['robin-yellow', 'Mal by si sa cítiť poctený, práve ideš bojovať s najvýznamenjšou figúrou novodobých dejín smetiska-lomítko-dediny.']
  ['you', 'Prečo? Čo urobil?']
  ['robin-yellow', 'To sa dozvieš sám, po tom, čo ho porazíš. Ber to ako motiváciu.']
]

talked = false
talk_go = ->
  if not talked
    talked = true
    for [author, msg] in dialog_go
      dialogbox.print(author, msg)

  cardbox.once('hide', talk_bye)

dialog_bye = [
  ['robin-yellow', 'Dobrá práca. Dúfam, že som ťa mojimi rečami nezmiatol a dúfam, že zistíš to, čo potrebuješ. A že to bude aj to, čo chceš.']
  ['robin-yellow', 'Hľadaj otázky, na ktoré odpoveď už dávno poznáš!']
  ['you', '...']
  ['robin-yellow', 'Veľa šťastia.']
]

talk_bye = ->
  for [author, msg] in dialog_bye
    dialogbox.print(author, msg)

  dialogbox.once('end', fly_away)

fly_away = ->
  robin.following.stop()

  dest = exports.away
  robin.flying.flyTo(dest.x, dest.y)

  robin.flying.once('end', next_stage)

next_stage = ->
  level.next()

exports.trigger = ->
  level = this
  dialogbox = @dialog
  cardbox = @cardbox
  robin = @robins.get('yellow')

  talk_go()

exports.away = null # set from map
  
