dialog = [
  ['you', 'Kde to sme? A prečo je tu tak veľa hračiek?']
  ['robin-blue', 'Sme ešte stále v dedine, nevidíš tie domy? A nie sú to hračky, hračky to boli. Kým neprišiel on a nerozhodol sa ich "vylepšiť". ']
  ['robin-blue', 'Pravda, že zo začiatku vylepšovanie bolo - ale potom sa to zvrhlo.']
  ['you', 'Zvrhlo? Ako to myslíš?']
  ['robin-blue', 'Hračky sa už nestávali lepšími a lepšími, naopak, len horšími a horšími. Zo začiatku im život dával, potom im ho ničil.']
  ['you', 'To ale nevysvetľuje, prečo je dedina v takom stave, v akom je!']
  ['robin-blue', 'To si len myslíš. Zmysel to dáva perfektný.']
  ['robin-blue', 'Hračky zamorili prostredie a umelý život vytlačil prírodu, ktorá tu mala právoplatné miesto.']
  ['robin-blue', 'Veď si to videl na vlastné oči, keď sme zachraňovali mojich súrodencou pred Zeleninou. ']
  ['robin-blue', 'Sme menšina, ktorá tu nemá čo hľadať.']
  ['robin-blue', 'Možno si aj ty časom uvedomíš, ako zlé to v skutočnosti je.']
  ['robin-blue', 'Poď, máš pred sebou ešte dlhú cestu!']
]

talked = false

exports.trigger = ->
  if not talked
    talked = true
    for [author, msg] in dialog
      @dialog.print(author, msg)
