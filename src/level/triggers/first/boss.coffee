level = null
dialogbox = null
cardbox = null
robin = null

dialog = [
  ['robin-blue', 'Je tam, vidíš ho?']
  ['you', 'Koho?']
  ['robin-blue', 'Predsa jeho! Je jedným z prvých experimentov, ktoré sa tu kedy pokazili. Adaptoval sa však až príliš dobre.']
  ['robin-blue', 'Priprav sa na krv, pot, slzy a ak si príliš veľká padavka, aj na ostatné telesné tekutiny.']
  ['robin-blue', 'Nenechaj sa zahanbiť!']
  ['robin-blue', 'A pozor na jeho fúzy.']
  ['you', 'Prečo si mám dávať pozor na--']
  ['robin-blue', 'Bež!']
]

talked = false
talk_go = ->
  if not talked
    talked = true
    for [author, msg] in dialog
      dialogbox.print(author, msg)

  cardbox.once('hide', talk_bye)

talk_bye = ->
  dialogbox.print('robin-blue', 'Vedel som, že to zvládneš! Aj bez zbytočných telesných tekutín!')
  dialogbox.print('robin-blue', 'Teraz ťa budem musieť opustiť. Neboj sa však, po boku ti bude lietať môj brat!')

  dialogbox.once('end', fly_away)

fly_away = ->
  robin.following.stop()

  dest = exports.away
  robin.flying.flyTo(dest.x, dest.y)

  robin.flying.once('end', next_stage)

next_stage = ->
  level.next()

exports.trigger = ->
  level = this
  dialogbox = @dialog
  cardbox = @cardbox
  robin = @robins.get('blue')

  talk_go()

exports.away = null # set from map
  
