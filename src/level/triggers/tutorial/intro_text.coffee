player = null
dialogbox = null

dialog = [
  'Kde vlastne si?'
  'Hlúpa otázka. Si v dedine. Dedine, ktorá vyzerá na prvý pohľad opustená a ty si nepamätáš, ako si sa v nej ocitol.'
  'Keď sme už pri tom, nepamätáš si vôbec nič. Pri pohľade na tvoje topánky si uvedomuješ, že si nepamätáš ani to, že by si si ich niekedy kupoval.'
  'Možno len vytesňuješ zlé spomienky. Tie topánky sú dosť zlé na to, aby to bola pravda.'
  'Mal by si radšej skúsiť prejsť sa dedinou. Použi šípky alebo pohni guličkou smerom chceš ísť.'
]

talk = ->
  for msg in dialog
    dialogbox.print('storyteller', msg)

  dialogbox.once('end', activate)

activate = ->
  player.keyboard.allow('walking')

exports.trigger = ->
  player = @player
  dialogbox = @dialog

  talk()

  delete exports.trigger
