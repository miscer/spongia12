player = null
robin = null
dialogbox = null

dialog = [
  ['robin-blue', 'Naša dedina nie je miesto pre malého chlapca ako ty! Čo tu robíš?']
  ['storyteller', 'Si príliš zaskočený na to, aby si zareagoval. Aj keď si asi stratil pamäť, si si skoro úplne istý, že červienky by rozprávať nemali.']
  ['robin-blue', '...']
  ['robin-blue', 'Ignorovať ostatných nie je slušné!']
  ['storyteller', 'Ale počiatočný šok si prekonal pomerne rýchlo. Do čerta s tým, si ešte malý, máš právo rozprávať sa s červienkami.']
  ['storyteller', 'Aspoň predpokladáš, že si malý. Nepamätáš si ani, koľko máš rokov.']
  ['you', 'Prepáčte mi.']
  ['robin-blue', 'Tykaj mi, cítim sa pri tebe starý. No keď inak nedáš, je ti odpustené. Čo tu teda robíš?']
  ['you', 'Nepamätám sa. Myslím, že som stratil pamäť.']
  ['robin-blue', 'To znie zle. Ale táto dedina naozaj pre deti aj tak nie je vhodná.']
  ['you', 'Prečo by nemala byť? Čo mi o nej vieš povedať?']
  ['robin-blue', 'O dedine sa nerozprávame! Bol tu niekto, niekto kto robil ostatným neslýchané. Vytváral veci, ktoré by existovať nikdy nemali. Ničil v aj drobné kúsky nádeje tým, že bol zvedavý.']
  ['robin-blue', 'Tu je to ešte v poriadku, žije nás tu málo. Všetci ušli preč. Horšie je to za hranicami.']
  ['storyteller', 'Mrazí ti krv v žilách, ale súčasne ťa rozprávanie púta každým slovom viac a viac.']
  ['robin-blue', 'Ale... Ty... Si mi povedomý. Oh, áno, si. Ty by si tu rozhodne nemal byť.']
  ['you', 'Naozaj? Kto vlastne som?']
  ['robin-blue', 'Oh môjty bože, nemôžeš sa červienok len tak pýtať kto si! To je predsa otázka, na ktorú odpoveď musíš nájsť sám, môj malý priateľ.']
]

notice = ->
  robin.noticing.notice(flyToPlayer)

flyToPlayer = ->
  pos = player.getPosition()
  robin.flying.flyTo(pos.x + 100, pos.y)

  robin.flying.once('end', talk)

talk = ->
  for [author, msg] in dialog
    dialogbox.print(author, msg)

  dialogbox.once('end', flyAway)

flyAway = ->
  dest = exports.robin_tree
  robin.flying.flyTo(dest.x, dest.y)
  robin.flying.once('end', sit)

  setTimeout(youShouldFollow, 2000)

sit = ->
  robin.drawing.sit('right')

youShouldFollow = ->
  dialogbox.print('storyteller', 'Tú červienku by si mal nasledovať.')

exports.trigger = ->
  player = @player
  robin = @robins.get('blue')
  dialogbox = @dialog

  player.walking.stop()

  notice()

  delete exports.trigger

exports.robin_tree = null # set from map
