level = null
player = null
robin = null
enemies = null
dialogbox = null
zones = null
skillbox = null
potions = null
cards = null
cardbox = null

follow_player = ->
  player.setRobin(robin)
  robin.following.start(player)

  talk_intro()

dialog_intro = [
  ['robin-blue', 'To sú moji malí súrodenci. Náš otec cez deň spí a nemá nás kto chrániť! Postavil by som sa im aj sám, ale pravdu povediac sa trochu bojím.']
  ['robin-blue', 'Bude stačiť, ak budeš pri mne stáť a tváriť sa ako morálna podpora! O útoky sa postarám sám.']
  ['storyteller', 'Hm. Prijateľnejšie ako zapredanie duše. Myslíš si, že by si to mohol zvládnuť!']
  ['storyteller', 'Najprv sa ale musíš naučiť, ako boj vlastne funguje. Vidíš to políčko v ľavom hornom rohu, označené s HP? To sú Health Points.']
  ['storyteller', 'V doslovnom preklade znamenajú niečo ako Životné body. Alebo Body života. Život udávajúce body? Mama mi vždy tvrdila, že na jazyky nemám talent. Asi mala pravdu. Nevadí! Označujú, ako veľa života ti ostáva. ']
  ['storyteller', 'Keď o všetky prídeš, umrieš. Dúfam, že vieš, čo znamná umrieť.']
  ['storyteller', 'Nad nepriateľom môžeš sledovať, aký je stav jeho HP.']
  ['storyteller', 'No späť k tebe! Vidíš to druhé políčko, označené SP? To sú Special Points. Udávajú, koľko bodov si využil pre špeciálny útok a koľko bodov ti ešte ostáva.']
  ['storyteller', 'To pod nimi ti prezrádza, ako veľa skúseností si nabral. Xp. je skratka pre experience, čo v preklade znamená skúsenosť.']
  ['storyteller', 'Čím viac príšer porazíš, tým viac xp. bodov získaš a tým sa ti časom zvýši level! S novým levelom prichádza veľa výhod. Ale o tom potom!']
  ['storyteller', 'Môže sa ti stať, že ti na porazenie nepriateľa tvoj level nebude stačiť. Neboj sa však! Budeš sa môcť vracať do máp, ktoré si už prešiel a ešte raz skántriť ich obyvateľov, čím si level budeš môcť zvýšiť.']
  ['storyteller', 'Boj ako taký nie je zložitý! Stačí stlačiť Q alebo kliknúť na tlačidlo Attack. Červienka bude namiesto teba útočiť na diaľku. Poď si to skúsiť.']
]

talk_intro = ->
  for [author, msg] in dialog_intro
    dialogbox.print(author, msg)

  dialogbox.once('end', kill_two)

kill_two = ->
  zones.activate()

  enemies.once 'dead', ->
    enemies.once('dead', talk_special)

dialog_special = [
  'Boj ti ide. Myslím, že by som ťa mohol naučiť aj niečo viac. Mal by som ťa ale varovať!'
  'Na to aby si to zvládol, musíš byť tichý ako les, ale s ohňom v duši! Prudký ako rozvírená rieka, so všetkou silou tajfúnu a zúriaceho ohňa. Tajomný ako odvrátená strana mesiaca!'
  'Dobre. Dúfam, že si pripravený.'
  'Stlač E alebo klikni na tlačidlo Special Attack! Zaútoč špeciálnym útokom! '
]

talk_special = ->
  zones.deactivate()
  robin.attacking.block()

  setTimeout ->
    for msg in dialog_special
      dialogbox.print('storyteller', msg)
  , 1000

  dialogbox.once('end', kill_one)

kill_one = ->
  zones.activate()
  robin.attacking.unblock()

  enemies.once('dead', talk_good_job)

talk_good_job = ->
  zones.deactivate()
  robin.attacking.block()

  setTimeout ->
    dialogbox.print('storyteller', 'Výborne! Ide ti to!')
  , 1000

  dialogbox.once('end', kill_rest)

kill_rest = ->
  zones.activate()
  robin.attacking.unblock()

  counter = 0

  dead = ->
    counter++

    if counter is 3
      talk_levels()
    else
      enemies.once('dead', dead)

  enemies.once('dead', dead)

dialog_levels = [
  'Skvelá práca! Zvýšil sa ti level! Ako som spomínal predtým, získal si množstvo výhod - ktoré si ale len teraz rozdelíš!'
  'Sú to Stat Points. Nazvyme ich Zdatnostné Body. Je to malý počet bodov, ktorý si teraz môžeš podľa vlastného uváženia rozdeliť medzi - a teraz dávaj pozor'
  'Útok, čím sa ti zvýši sila útoku a budeš protivníkom uberať viac bodov. Vitalitu, čím sa ti zvýši obrana (protivníci ti budú uberať menej bodov) a maximálna hodnota HP.'
  'Môžeš vypomôcť červienke. Keď si dáš body na ňu, zvýši sa ti maximálne hodnota SP a tvoj špeciálny útok bude uberať viac bodov.'
  'Je tu aj rýchlosť, vďaka ktorej budeš útočiť rýchlejšie, v kratších časových intervaloch!'
  'A v neposlednej rade, šťastie. Zvýši sa ti šanca, že protivníkov útok zablokuješ, alebo že na neho zaútočíš dva razy za sebou namiesto jedného!'
]

talk_levels = ->
  setTimeout ->
    for msg in dialog_levels
      dialogbox.print('storyteller', msg)
  , 1000

  dialogbox.once('end', show_skillbox)

show_skillbox = ->
  skillbox.show()
  skillbox.once('hide', talk_potions)

dialog_potions = [
  'Rád vidím, že si to zvládol. Teraz, vidíš tie predmety, čo sa povaľujú na zemi? Najprv začnime chemikáliami, ktoré vidíš v bankách pred sebou. Sú skvelá vec, ktorú som ti na začiatku zabudol spomenúť. '
  'Za normálnych okolností by som ti neradil piť nič, čo nájdeš na zemi, ale zúfalé časy vyžadujú zúfalé riešenia.'
  'Červená chemikália ti doplní HP a modrá SP.'
  'Prejdi k nim a zober ich.'
]

talk_potions = ->
  pos = player.getPosition()

  potions.spawn('hp', pos.x - 130, pos.y + 100)
  potions.spawn('hp', pos.x - 170, pos.y + 100)
  potions.spawn('sp', pos.x - 210, pos.y + 100)
  cards.spawn(0, pos.x - 300, pos.y + 70)

  for msg in dialog_potions
    dialogbox.print('storyteller', msg)

  pp = player.potions
  pp.on 'update', ->
    if pp.health is 2 and pp.special is 1
      talk_cards()

dialog_cards = [
  'Keď ich raz budeš mať, budú sa ti zobrazovať v pravom doľnom rohu. '
  'Keď budeš chcieť vypiť červenú chemikáliu, stlač X alebo klikni na jej ikonku v pravom doľnom rohu.'
  'Keď budeš chcieť vypiť modrú chemikáliu, stlač C alebo klikni na jej ikonku v pravom doľnom rohu.'
  'A pre tvoje vlastné dobro sa nepýtaj, z čoho sú vyrobené.'
  'No a teraz! Vidíš tú kartičku? Prejdi k nej a zodvihni ju. Povie ti bližšie informácie o tvorovi, ktorého si porazil!'
]

talk_cards = ->
  for msg in dialog_cards
    dialogbox.print('storyteller', msg)

  cardbox.once('hide', talk_final)

dialog_final = [
  ['storyteller', 'Dúfam, že si si ju prečítal. Text na nej ti môže prinavrátiť spomienky, ktoré si stratil. Môže aj nemusí, ale si si istý, že chceš skúšať náhodu?']
  ['robin-blue', 'Zachránil si ma a mojich súrodencov! Predsa len je v tebe aj kus dobra! Ako som sľúbil, sme tvojími dlžníkmi. ']
  ['robin-blue', 'Odteraz sa na nás môžeš spoľahnúť.']
  ['robin-blue', 'Sme statočnejší, než vyzeráme! Preto ťa jeden z nás bude vždy nasledovať a pomáhať ti, keď to budeš potrebovať. Tvoja cesta bude dlhá a zložitá, ale s našou pomocou to zvládneš.']
  ['robin-blue', 'Máme však ešte jednu podmienku.']
  ['storyteller', 'Teraz to príde. Teraz sa budeš musieť vzdať svojho prvorodeného syna.']
  ['robin-blue', 'Musíš sa zbaviť rozprávača. Buď my alebo on. Vysvetlil ti ako sa hra ovláda a tam jeho cesta končí!']
  ['storyteller', 'Váhaš. Si na pochybách. Ale nesmieš! Po tom všetkom, čím sme si prešli, nesmieš! Ja predsa neučím bojovať len tak niekoho, ty si bol výnimočný!']
  ['you', '...']
  ['you', 'Mrzí ma to.']
  ['you', 'Chyba nebola v tebe, ale vo mne.']
  ['storyteller', 'Ako sa ma môžeš tak ľahko vzd']
  ['robin-blue', 'Vymazal som ho za teba, ak ti to nevadí.']
  ['you', 'To je v poriadku, ďakujem.']
  ['robin-blue', 'V tom prípade môžeme začať!']
]

talk_final = ->
  for [author, msg] in dialog_final
    dialogbox.print(author, msg)

  dialogbox.once('end', level.next)

exports.trigger = ->
  level = this
  player = @player
  robin = @robins.get('blue')
  enemies = @enemies
  dialogbox = @dialog
  zones = @enemyZones
  skillbox = @skillbox
  potions = @potions
  cards = @cards
  cardbox = @cardbox

  follow_player()

  delete exports.trigger
