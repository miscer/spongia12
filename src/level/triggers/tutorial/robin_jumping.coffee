robin = null
dialogbox = null

dialog = [
  ['robin-blue', 'Ako si sa sem dostal?']
  ['you', 'Naučil som sa skákať.']
  ['robin-blue', 'To je šikovné, raz by si ma to mal naučiť tiež!']
  ['storyteller', 'Nie, nemal. Skákanie je niečo výnimočné, čo nás dvoch spája. Nikto iný o tom nesmie vedieť.']
  ['you', 'Ehm. Je to príliš zložité, nemyslím si, že by som ťa to dokázal naučiť.']
  ['robin-blue', 'Naozaj? Škoda. Tak či tak, čo odomňa chceš?']
  ['you', 'Chcem vedieť kto som!']
  ['robin-blue', 'Už som ti povedal, že ti to nesmiem prezradiť! Nechaj ma tak!']
]

notice = ->
  robin.drawing.sit('right')
  robin.noticing.notice(talk)

talk = ->
  for [author, msg] in dialog
    dialogbox.print(author, msg)

  dialogbox.once('end', flyAway)

flyAway = ->
  dest = exports.robin_offended
  robin.flying.flyTo(dest.x, dest.y)

exports.trigger = ->
  robin = @robins.get('blue')
  dialogbox = @dialog

  notice()

  delete exports.trigger

exports.robin_offended = null # set from map
