exports.trigger = ->
  robin = @robins.get('blue')
  dest = exports.robin_explain

  robin.flying.flyTo(dest.x, dest.y)

  delete exports.trigger

exports.robin_explain = null # set from map
