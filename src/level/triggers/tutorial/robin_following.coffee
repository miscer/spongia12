robin = null
dialogbox = null

dialog = [
  ['you', 'Tak mi aspoň pomôž to zistiť.']
  ['robin-blue', 'Hm... Mohol by som ti pomôcť. Otec ma učil ľuďom pomáhať. Ale iba ak mi pomôžeš ako prvý!']
  ['storyteller', 'Váhaš. A máš prečo. Červienky su kruté, nemilosrdné tvory, ktoré ťa s najväčšou pravdepodobnosťou donútia zapredať im dušu tvojho prvorodeného syna.']
  ['storyteller', 'Ale ty asi ešte ani nevieš, ako sa deti dostávajú na svet, čiže by si jeho ponuku mal priať.']
  ['you', 'Dobre, urobím čo len chceš!']
  ['robin-blue', 'Skvelé, nasleduj ma.']
]

talk = ->
  robin.drawing.fly('left')

  for [author, msg] in dialog
    dialogbox.print(author, msg)

  dialogbox.once('end', flyAway)

flyAway = ->
  dest = exports.robin_following
  robin.flying.flyTo(dest.x, dest.y)

exports.trigger = ->
  robin = @robins.get('blue')
  dialogbox = @dialog

  talk()

  delete exports.trigger
