player = null
robin = null
dialogbox = null

dialog = [
  ['robin-blue', 'Ty si ma nasledoval! No dobre, poď hore za mnou, keď inak nedáš.']
  ['you', 'Ale... ja skákať neviem.']
  ['robin-blue', 'Nič si z toho nerob, ani ja. Preto lietam. Skús to!']
  ['you', '...']
  ['you', 'Myslím si, že lietať nebudem vedieť.']
  ['robin-blue', 'Oh, to je vskutku škoda. Tak nič.']
]

notice = ->
  robin.noticing.notice(talk)

talk = ->
  for [author, msg] in dialog
    dialogbox.print(author, msg)

  dialogbox.once('end', turnAway)

turnAway = ->
  robin.drawing.sit('left')

  setTimeout(activate, 1500)

activate = ->
  dialogbox.print('storyteller', 'Ešte, že tu máš mňa. Neboj sa, kamoš, skákať môžeš šípkou hore, prípadne pohnutím guličky hore.')
  player.keyboard.allow('jumping')

exports.trigger = ->
  player = @player
  robin = @robins.get('blue')
  dialogbox = @dialog

  notice()

  delete exports.trigger
