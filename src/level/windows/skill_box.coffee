love = require 'lovejs'
bond = require 'bond'

Window = require './window'

class SkillBox extends Window
  LEVELS = ['attack', 'vitality', 'robin', 'speed', 'luck']

  constructor: ->
    super(400, 300)

    @asset = love.assets.addImage('images/level/skillbox.png')
    love.assets.addImage('images/level/skillbox_buttons.png')

    @buttons = {}
    y = @y + 95
    x = @x + 340
    for level in LEVELS
      @buttons[level] = btn = bond.gui.newButton('+', x, y, 'add-level')
      btn.on('click', @add.bind(this, level))
      y += 32

    @buttons.done = bond.gui.newButton('done', @x + 160, @y + 260, 'levels-done')
    @buttons.done.on('click', @hide)

  attached: ->
    @player = @parent.getComponent('player')
    @levels = @player.levels

  init: ->
    super
    @background = love.graphics.newImage(@asset)

  show: =>
    super

    @levels.on('update', @lupdate)
    @redraw()

    for name, button of @buttons
      bond.gui.add(button)

  hide: =>
    @levels.removeListener('update', @lupdate)

    for name, button of @buttons
      bond.gui.remove(button)

    super

  lupdate: =>
    @rebutton()
    @redraw()

  rebutton: ->
    for level in LEVELS
      button = @buttons[level]

      if button? and @levels.isMax(level)
        delete @buttons[level]
        bond.gui.remove(button)

  redraw: ->
    love.graphics.setCanvas(@canvas)

    love.graphics.draw(@background, @x, @y)

    love.graphics.setColor(0xa2, 0x92, 0x7a)
    love.graphics.setFont('Pixel', 12)
    love.graphics.print(@levels.points, @x + 275, @y + 44)

    love.graphics.setColor(0xf8, 0xe2, 0xbf)
    love.graphics.setFont('Pixel', 20)

    y = @y + 112
    for level in LEVELS
      love.graphics.print(@levels.get(level), @x + 220, y)

      if not @levels.isMax(level)
        love.graphics.print(@levels.getCost(level), @x + 300, y)

      y += 32

    love.graphics.setCanvas()

  add: (level) ->
    cost = @levels.getCost(level)

    if @levels.points >= cost
      @levels.points -= cost
      @levels.up(level)

module.exports = SkillBox
