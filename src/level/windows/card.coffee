love = require 'lovejs'

Window = require './window'

class CardWindow extends Window
  constructor: ->
    super(650, 570)

    @assets = []
    for i in [0..4]
      @assets[i] = love.assets.addImage("images/cards/lvl#{i}.png")

  init: ->
    super

    @images = []
    for asset, i in @assets
      @images[i] = love.graphics.newImage(asset)

  show: (n) ->
    love.graphics.setCanvas(@canvas)
    love.graphics.draw(@images[n], @x, @y)
    love.graphics.setCanvas()

    love.keyboard.once('pressed', @hide)

    touch = =>
      window.removeEventListener('touchstart', touch)
      @hide()

    window.addEventListener('touchstart', touch)

    super

  hide: =>
    super

module.exports = CardWindow
