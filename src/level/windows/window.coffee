love = require 'lovejs'
bond = require 'bond'

_canvas = null

class Window extends bond.components.Component
  constructor: (@width, @height) ->
    w = love.graphics.getWidth()
    h = love.graphics.getHeight()

    @x = (w - @width) / 2
    @y = (h - @height) / 2

    love.eventify(this)

  show: ->
    @trigger('show')
    @parent.pause()
    @canvas.style.display = 'block'

  hide: ->
    @canvas.style.display = 'none'

    love.graphics.setCanvas(@canvas)
    love.graphics.clear()
    love.graphics.setCanvas()

    @parent.resume()
    @trigger('hide')

  init: ->
    @canvas = @getCanvas()

  getCanvas: ->
    if _canvas is null
      el = document.getElementById('windows')
      el.width = love.graphics.getWidth()
      el.height = love.graphics.getHeight()
      _canvas = love.graphics.newCanvas(el)

    _canvas


module.exports = Window
