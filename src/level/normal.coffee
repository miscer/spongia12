Level = require './index'

class NormalLevel extends Level
  constructor: ->
    super

    color = @map.getProperty('robin')
    robin = @robins.get(color)

    @player.setRobin(robin)
    robin.following.start(@player)

    @player.levels.on('mainUp', @skillbox.show)

  init: ->
    super
    @player.getRobin().following.jumpToPlayer()

module.exports = NormalLevel
