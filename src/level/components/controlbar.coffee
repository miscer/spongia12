love = require 'lovejs'
bond = require 'bond'

class Controlbar extends bond.components.Component
  constructor: (x, y) ->
    @bgAsset = love.assets.addImage('images/level/controlbar_background.png')
    @ballAsset = love.assets.addImage('images/level/controlbar_ball.png')

    love.assets.addImage('images/level/controlbar_buttons.png')

    @ball =
      aabb: new bond.collisions.AABB(72, 0, 166, 80)
      center: new bond.vectors.Vector(120, 40)
      distance: new bond.vectors.Vector
      left: new bond.vectors.Vector(-1, 0)
      quad: love.graphics.newQuad()
      position: 0
      touch: null

    @ball.aabb.move(x, y)
    @ball.center.add(x, y)

    @buttons =
      attack: bond.gui.newButton('attack', 388 + x, y, 'attack')
      special: bond.gui.newButton('special', 216 + x, y, 'special')
      hpot: bond.gui.newButton('0', 588 + x, y, 'hpot')
      spot: bond.gui.newButton('0', 758 + x, y, 'spot')

    @x = x
    @y = y

  attached: ->
    @player = @parent.player
    @player.potions.on('update', @updateButtons)

    @buttons.hpot.on('click', @player.potions.useHealth)
    @buttons.spot.on('click', @player.potions.useSpecial)
    @buttons.attack.on('click', @player.normalAttack)
    @buttons.special.on('click', @player.specialAttack)

  init: ->
    @canvas = love.graphics.newCanvas(960, 80)
    @background = love.graphics.newImage(@bgAsset)
    @ball.image = love.graphics.newImage(@ballAsset)

    el = love.graphics.getCanvas().el

    el.addEventListener('touchstart', @tstart)
    el.addEventListener('touchmove', @tmove)
    el.addEventListener('touchend', @tend)

    for name, button of @buttons
      bond.gui.add(button)

  clear: ->
    el = love.graphics.getCanvas().el

    el.removeEventListener('touchstart', @tstart)
    el.removeEventListener('touchmove', @tmove)
    el.removeEventListener('touchend', @tend)

    for name, button of @buttons
      bond.gui.remove(button)

  draw: ->
    love.graphics.draw(@background, @x, @y)

    @drawBall()

  updateButtons: =>
    @buttons.hpot.setText(@player.potions.health)
    @buttons.spot.setText(@player.potions.special)

  # --- ball stuff ---

  tstart: (e) =>
    touch = e.changedTouches[0]
    if @isTouchingBall(touch)
      @ball.touch = touch.identifier

    e.preventDefault()

  tmove: (e) =>
    touch = null

    for t in e.changedTouches
      if t.identifier is @ball.touch
        touch = t
        break

    if touch?
      @updateBall(touch.pageX, touch.pageY)

    e.preventDefault()

  tend: (e) =>
    touch = e.changedTouches[0]
    if touch.identifier is @ball.touch
      @ball.touch = null
      @setBallPosition(0)

    e.preventDefault()

  isTouchingBall: (touch) ->
    x = touch.pageX
    y = touch.pageY

    @ball.aabb.includes(x, y)

  MIN2 = 600

  updateBall: (x, y) ->
    @ball.distance.update(x, y).vsubtract(@ball.center)

    if @ball.distance.length2() < MIN2
      @setBallPosition(0)
      return

    angle = bond.vectors.Vector.angle(@ball.left, @ball.distance) * 180 / Math.PI
    pos = 0

    if @ball.distance.y < 0
      if 0 < angle <= 22.5
        pos = 1
      else if 22.5 < angle <= 67.5
        pos = 2
      else if 67.5 < angle <= 112.5
        pos = 3
      else if 112.5 < angle <= 157.5
        pos = 4
      else if 157.5 < angle <= 180
        pos = 5
    else
      if 0 < angle <= 22.5
        pos = 1
      else if 157.5 < angle <= 180
        pos = 5

    @setBallPosition(pos)

  setBallPosition: (pos) ->
    prev = @ball.position
    @ball.position = pos

    if pos in [1, 2] and prev in [0, 3, 4, 5]
      @player.walking.start(-1)
    else if pos in [4, 5] and prev in [0, 1, 2, 3]
      @player.walking.start(1)
    else if pos in [0, 3] and prev in [1, 2, 4, 5]
      @player.walking.stop()

    if pos in [2, 3, 4] and prev in [0, 1, 5]
      @player.jumping.jump()

  # position -> image
  BALL_MAPPING =
    0: 0 # center
    1: 3 # left
    2: 6 # top left
    3: 1 # top
    4: 5 # top right
    5: 4 # right

  drawBall: ->
    @ball.quad.setViewport(
      BALL_MAPPING[@ball.position] * 80,
      0, 80, 80
    )

    love.graphics.drawq(@ball.image, @ball.quad, @x + 78, @y)

module.exports = Controlbar
