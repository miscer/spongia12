bond = require 'bond'

class Collisions extends bond.components.Component
  constructor: ->
    @manager = new bond.collisions.Manager(30)

  add: (collider) ->
    @manager.add(collider)

  remove: (collider) ->
    @manager.remove(collider)

  update: ->
    @manager.update()

module.exports = Collisions
