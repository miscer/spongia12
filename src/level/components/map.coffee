love = require 'lovejs'
{tiled, collisions, components} = require 'bond'

class Map extends components.Component
  constructor: (json) ->
    tilesets = {}
    for tileset in json.tilesets
      tilesets[tileset.name] = love.assets.addImage('images/' + tileset.properties.image).getContent()

    @tiled = tiled.new(json, tilesets)

    @platforms = @tiled.layer('platforms.objects')
    @entities = @tiled.layer('entities')

  attached: ->
    @configurePlayer()
    @configurePlatforms()
    @configureTriggers()
    @configureEnemies()
    @configureRobins()
    @configurePotions()

  configurePlayer: ->
    player = @parent.getComponent('player')
    robins = @parent.getComponent('robins')

    obj = @entities.get('player')
    player.setPosition(obj.x, obj.y)

  configurePlatforms: ->
    platforms = @parent.getComponent('platforms')
    zones = @parent.getComponent('enemyZones')

    for obj in @platforms.objects
      platforms.add(obj.x, obj.y, obj.width, obj.height)
      zones.add(obj.x, obj.y, obj.width)

    return

  configureTriggers: ->
    triggers = @parent.getComponent('triggers')

    if @tiled.layer('triggers')?
      for obj in @tiled.layer('triggers').objects
        triggers.set(obj.name, obj.x, obj.y, obj.width, obj.height)

    if @tiled.layer('triggers.positions')?
      for obj in @tiled.layer('triggers.positions').objects
        props = obj.properties
        triggers.get(props.trigger)[obj.name] = {x: obj.x, y: obj.y}

    return

  configureEnemies: ->
    enemies = @parent.getComponent('enemies')
    stage = parseInt(@getProperty('stage'), 10)

    for obj in @entities.type('enemy')
      enemies.spawn(obj.name, stage, obj.x, obj.y)

    for obj in @entities.type('boss')
      enemies.boss(obj.name, obj.x, obj.y)

    return

  configureRobins: ->
    robins = @parent.getComponent('robins')

    for obj in @entities.type('robin')
      robins.spawn(obj.name, obj.x, obj.y)

    return

  configurePotions: ->
    potions = @parent.getComponent('potions')

    for obj in @tiled.layer('objects').type('potion')
      potions.spawn(obj.name, obj.x, obj.y)

    return

  init: ->
    @background = @tiled.layer('platforms.tiles').draw()

    @initRobins()

  initRobins: ->
    robins = @parent.getComponent('robins')

    for obj in @entities.type('robin')
      robin = robins.get(obj.name)
      props = obj.properties

      if props.fn?
        robin.drawing[props.fn](props.arg)

    return

  draw: ->
    love.graphics.draw(@background, 0, 0)

  getWidth: ->
    @tiled.width

  getHeight: ->
    @tiled.height

  getProperty: (name) ->
    @tiled.json.properties[name]

module.exports = Map
