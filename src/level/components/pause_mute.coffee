love = require 'lovejs'
bond = require 'bond'

class PauseMute extends bond.components.Component
  constructor: ->
    love.assets.addImage('images/level/pause_mute.png')

    @buttons =
      pause: bond.gui.newButton('pause', 894, 20, 'pause')
      mute: bond.gui.newButton('mute', 918, 20, 'mute')

    @buttons.pause.on('click', @pause)

  init: ->
    for name, button of @buttons
      bond.gui.add(button)

  clear: ->
    for name, button of @buttons
      bond.gui.remove(button)

  pause: =>
    if not @parent.isPaused()
      @parent.pause()
      @buttons.pause.setClass('pause active')
    else
      @parent.resume()
      @buttons.pause.setClass('pause')

module.exports = PauseMute
