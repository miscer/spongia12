love = require 'lovejs'
bond = require 'bond'

class Potions extends bond.components.Component
  constructor: ->
    @potions = []

    @assets =
      health: love.assets.addImage('images/level/pothp.png')
      special: love.assets.addImage('images/level/potsp.png')

  init: ->
    @images =
      health: love.graphics.newImage(@assets.health)
      special: love.graphics.newImage(@assets.special)

    for potion in @potions
      potion.setImage(@images[potion.type])

  spawn: (type, x, y) ->
    klass = @getClass(type)
    potion = new klass(x, y, this, @parent.player)

    @add(potion)
    
  getClass: (type) ->
    switch type
      when 'hp' then HealthPotion
      when 'sp' then SpecialPotion

  add: (potion) ->
    @parent.collisions.add(potion.collider)
    @potions.push(potion)

    if @images?
      potion.setImage(@images[potion.type])

  remove: (potion) ->
    @parent.collisions.remove(potion.collider)

    index = @potions.indexOf(potion)
    @potions.splice(index, 1)

  draw: ->
    for i in [@potions.length-1..0] by -1
      potion = @potions[i]
      potion.draw()

module.exports = Potions

class Potion
  constructor: (x, y, @potions, @player) ->
    @collider = new bond.collisions.Collider
    @collider.setDim(28, 28)
    @collider.setPos(x, y)
    @collider.collision = @collision

  setImage: (@image) ->

  collision: (other) =>
    if other.hasTag('player')
      @potions.remove(this)
      @add()

  draw: ->
    pos = @collider.getPos()
    love.graphics.draw(@image, pos.x, pos.y)

class HealthPotion extends Potion
  type: 'health'

  add: ->
    @player.potions.addHealth()

class SpecialPotion extends Potion
  type: 'special'

  add: ->
    @player.potions.addSpecial()
