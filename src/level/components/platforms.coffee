bond = require 'bond'

class Platforms extends bond.components.Component
  add: (x, y, w, h) ->
    collider = new bond.collisions.Collider
    collider.addTag('solid', 'platform')
    collider.setPos(x, y)
    collider.setDim(w, h)
    collider.freeze()

    @parent.collisions.add(collider)

module.exports = Platforms
