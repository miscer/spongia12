love = require 'lovejs'
bond = require 'bond'

class Cards extends bond.components.Component
  constructor: ->
    @cards = []

    @asset = love.assets.addImage('images/level/card.png')

  init: ->
    image = love.graphics.newImage(@asset)
    reel = new bond.animation.Reel(image, 38, 52)
    @animation = new bond.animation.Animation(reel, 0.5, [1, 2])

  spawn: (num, x, y) ->
    card = new Card(x, y, num, this, @parent.player)

    @add(card)
    
  add: (card) ->
    @parent.collisions.add(card.collider)
    @cards.push(card)

    card.setAnimation(@animation)

  remove: (card) ->
    @parent.collisions.remove(card.collider)

    index = @cards.indexOf(card)
    @cards.splice(index, 1)

  update: (dt) ->
    @animation.update(dt)

  draw: ->
    for i in [@cards.length-1..0] by -1
      card = @cards[i]
      card.draw()

module.exports = Cards

class Card
  constructor: (x, y, @num, @cards, @player) ->
    @collider = new bond.collisions.Collider
    @collider.setDim(38, 52)
    @collider.setPos(x, y)
    @collider.collision = @collision

  setAnimation: (@animation) ->

  collision: =>
    @cards.remove(this)
    @cards.parent.cardbox.show(@num)

  draw: ->
    pos = @collider.getPos()
    love.graphics.draw(@animation, pos.x, pos.y)
