love = require 'lovejs'
bond = require 'bond'

class Background extends bond.components.Component
  BACKGROUNDS =
    static:
      image: 'images/level/background_static.png'
      offset: {x: 0, y: -300}
      scale: 0.3
    back:
      image: 'images/level/background_back.png'
      offset: {x: 0, y: 100}
      scale: 0.6
    front:
      image: 'images/level/background_front.png'
      offset: {x: 200, y: 1246}
      scale: 1

  constructor: ->
    @last = {x: null, y: null}
    @assets = {}

    for name, bg of BACKGROUNDS
      @assets[name] = love.assets.addImage(bg.image)

  init: ->
    @layers = {}

    for name, asset of @assets
      @layers[name] = love.graphics.newImage(asset)

    el = document.getElementById('background')
    el.width = love.graphics.getWidth()
    el.height = love.graphics.getHeight()
    @canvas = love.graphics.newCanvas(el)

  draw: ->
    love.graphics.setCanvas(@canvas)
    love.graphics.clear()

    offset = @parent.camera.getOffset()

    for name, bg of BACKGROUNDS
      x = bg.offset.x - (offset.x * bg.scale)
      y = bg.offset.y - (offset.y * bg.scale)

      love.graphics.draw(@layers[name], x, y)

    love.graphics.setCanvas()

  clear: ->
    love.graphics.setCanvas(@canvas)
    love.graphics.clear()
    love.graphics.setCanvas()

module.exports = Background
