love = require 'lovejs'
bond = require 'bond'

Enemy = require '../entities/enemy'
Boss = require '../entities/boss'

class Enemies extends bond.components.Component
  ENEMIES =
    tomato:
      url: 'images/enemies/tomato.png'
      width: 80
      height: 104
    carrot:
      url: 'images/enemies/carrot.png'
      width: 80
      height: 140
    snabbage:
      url: 'images/enemies/snabbage.png'
      width: 100
      height: 160
    doll:
      url: 'images/enemies/doll.png'
      width: 80
      height: 170
    dwarf:
      url: 'images/enemies/dwarf.png'
      width: 100
      height: 160
    jose:
      url: 'images/enemies/jose.png'
      width: 100
      height: 120
    penguin:
      url: 'images/enemies/penguin.png'
      width: 80
      height: 80
    unicorn:
      url: 'images/enemies/unicorn.png'
      width: 200
      height: 200

  constructor: ->
    @enemies = []

    @assets = {}
    for name, props of ENEMIES
      @assets[name] = love.assets.addImage(props.url)

    love.eventify(this)

  init: ->
    @reels = {}
    for name, props of ENEMIES
      image = love.graphics.newImage(@assets[name])
      @reels[name] = new bond.animation.Reel(image, props.width, props.height)

    for enemy in @enemies
      enemy.drawing.setReel(@reels[enemy.name])
      enemy.init()

  spawn: (type, stage, x, y) ->
    enemy = new Enemy(type)
    enemy.setParent(this)
    enemy._createComponents()
    enemy.setPosition(x, y)

    props = ENEMIES[type]
    enemy.setDimensions(props.width, props.height)

    enemy.mechanics.setStage(stage)

    @enemies.push(enemy)

  boss: (name, x, y) ->
    boss = new Boss(name)
    boss.setParent(this)
    boss._createComponents()
    boss.setPosition(x, y)

    props = ENEMIES[name]
    boss.setDimensions(props.width, props.height)

    @boss = boss
    @enemies.push(boss)

  get: (i) ->
    @enemies[i]

  getBoss: ->
    @boss

  remove: (enemy) ->
    index = @enemies.indexOf(enemy)

    if index > -1
      @enemies.splice(index, 1)

  update: (dt) ->
    for i in [@enemies.length-1..0] by -1
      enemy = @enemies[i]
      enemy.update?(dt)

  draw: ->
    for enemy in @enemies
      enemy.draw?()

  getLevel: ->
    @parent

module.exports = Enemies
