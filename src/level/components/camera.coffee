bond = require 'bond'

class Camera extends bond.components.Component
  constructor: ->
    @camera = new bond.Camera(960, 520)

  update: ->
    pos = @parent.player.getPosition()
    aabb = @parent.player.getAABB()

    x = pos.x + (aabb.getWidth() / 2)
    y = pos.y + (aabb.getHeight() / 2)

    @camera.lookAt((x + 0.5) | 0, (y + 0.5) | 0)
    @camera.fix(0, 0, @parent.map.getWidth(), @parent.map.getHeight())

    if not @isVisible(aabb)
      @parent.dead()

  translate: ->
    @camera.translate()

  restore: ->
    @camera.restore()

  getOffset: ->
    @camera.getOffset()

  isVisible: (aabb) ->
    @camera.isVisible(aabb)

module.exports = Camera
