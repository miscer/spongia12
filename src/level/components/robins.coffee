bond = require 'bond'

Robin = require '../entities/robin'

class Robins extends bond.components.Component
  constructor: ->
    @instances = {}

  spawn: (color, x, y) ->
    robin = @get(color)
    robin.setPosition(x, y)

  get: (color) ->
    if not @instances[color]?
      robin = @instances[color] = new Robin(color)
      robin.setParent(this)
      robin._createComponents()
    @instances[color]

  init: ->
    for name, robin of @instances
      robin.init?()

  update: (dt) ->
    for name, robin of @instances
      robin.update?(dt)

  draw: ->
    for name, robin of @instances
      robin.draw?()

  getLevel: ->
    @parent

module.exports = Robins
