bond = require 'bond'

class Triggers extends bond.components.Component
  constructor: ->
    @triggered =
      prev: []
      now: []

  get: (name) ->
    triggers[name]

  set: (name, x, y, w, h) ->
    if not triggers[name]
      throw new Error("there is no trigger called #{name}")

    c = new bond.collisions.Collider
    c.setPos(x, y)
    c.setDim(w, h)
    c.addTag('trigger', name)
    c.collision = @trigger.bind(this, name)

    @parent.collisions.add(c)

  update: ->
    @triggered.prev = @triggered.now
    @triggered.now = []

  trigger: (name, other) ->
    if other.hasTag('player')
      if name not in @triggered.prev
        triggers[name].trigger?.call(@parent, other.getObject())

      @triggered.now.push(name)

    return

triggers = {}

names = [
  'tutorial/intro_text', 'tutorial/robin_first', 'tutorial/jumping_problem',
  'tutorial/double_jumping_problem', 'tutorial/robin_jumping',
  'tutorial/robin_following', 'tutorial/robin_following_continue', 'tutorial/fighting'

  'first/start', 'first/stage_2', 'first/boss'

  'second/start', 'second/stage_4', 'second/boss'

  'third/start', 'third/stage_6', 'third/boss'

  'fourth/start', 'fourth/stage_8', 'fourth/boss'

  'fifth/start', 'fifth/stage_10', 'fifth/boss'
]

for name in names
  triggers[name] = require("../triggers/#{name}")

module.exports = Triggers
