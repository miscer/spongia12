bond = require 'bond'

class EnemyZones extends bond.components.Component
  constructor: ->
    @active = true
    @zones = []

  add: (x, y, w) ->
    if w < 150
      return

    zone = new Zone(x, y, w)
    @parent.collisions.add(zone.collider)

    @zones.push(zone)

  activate: ->
    @active = true

  deactivate: ->
    @active = false

  update: ->
    if not @active
      return

    for zone in @zones
      zone.update()

    return

module.exports = EnemyZones

class Zone
  constructor: (x, y, w) ->
    @collider = new bond.collisions.Collider
    @collider.setDim(w, 100)
    @collider.setPos(x, y)
    @collider.collision = @collision

    @player = null
    @enemy = null

  update: ->
    if @player? and @enemy?
      @enemy.chasing.chase(@player)

    @player = null
    @enemy = null

  collision: (other) =>
    if other.hasTag('enemy')
      @enemy = other.getObject()

    if other.hasTag('player')
      @player = other.getObject()
