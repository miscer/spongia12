bond = require 'bond'

class Fighting extends bond.components.Component
  constructor: ->
    @fights = []

  attached: ->
    @player = @parent.getComponent('player')

  new: (enemy) ->
    fight = new Fight(@player, enemy)
    @fights.push(fight)
    fight

  update: (dt) ->
    for fight in @fights
      fight.update(dt)

module.exports = Fighting



class Fight
  MIN_DISTANCE = 100
  MIN_DISTANCE2 = Math.pow(MIN_DISTANCE, 2)

  constructor: (@fighter1, @fighter2) ->
    @timer1 = 0
    @timer2 = 0

    @distance = new bond.vectors.Vector

  update: (dt) ->
    if not @areCloseEnough()
      return

    f1 = @fighter1.fighting
    f2 = @fighter2.fighting

    if f1.canAttack()
      f1.attack(@fighter2)

    if f2.canAttack()
      f2.attack(@fighter1)

  areCloseEnough: ->
    aabb1 = @fighter1.getAABB()
    aabb2 = @fighter2.getAABB()

    aabb1.distanceTo(aabb2, @distance) # this changes @distance

    @distance.length2() <= MIN_DISTANCE2



