love = require 'lovejs'
bond = require 'bond'

state = require '../../state'

class Infobox extends bond.components.Component
  BARS =
    width: 160
    height: 12
    source:
      hp: [0, 0]
      sp: [0, 12]
      xp: [0, 24]
    dest:
      hp: [128, 24]
      sp: [128, 44]
      xp: [128, 64]

  constructor: (@points) ->
    @background = love.assets.addImage('images/level/infobox.png')
    @bars = love.assets.addImage('images/level/infobox_bars.png')

  attached: ->
    @points = @parent.getComponent('player').points
    @levels = @parent.getComponent('player').levels

  init: ->
    @background = love.graphics.newImage(@background)
    @bars = love.graphics.newImage(@bars)

    @canvas = love.graphics.newCanvas(@background.width, @background.height)

    love.graphics.setCanvas(@canvas)
    love.graphics.setColor(0xff, 0xff, 0xff)
    love.graphics.setTextBaseline('top')
    love.graphics.setCanvas()

  redraw: =>
    if not @canvas?
      return
    
    love.graphics.setCanvas(@canvas)

    love.graphics.draw(@background, 0, 0)

    love.graphics.setFont('Pixel', 36)
    love.graphics.print(@levels.main, 30, 20)

    love.graphics.setFont('Pixel', 16)
    love.graphics.print(state.levels.current, 200, -2)

    @drawProgressBar('hp', @points.get('health'), @points.max('health'))
    @drawProgressBar('sp', @points.get('special'), @points.max('special'))
    @drawProgressBar('xp', @points.get('experience'), @points.max('experience'))

    love.graphics.setCanvas()

  drawProgressBar: (type, points, max) ->
    ratio = points / max
    width = BARS.width * ratio
    source = BARS.source[type]
    dest = BARS.dest[type]
    quad = love.graphics.newQuad(source[0], source[1], width, BARS.height)

    if width > 0
      love.graphics.drawq(@bars, quad, dest[0], dest[1])

  draw: ->
    @redraw()
    love.graphics.draw(@canvas, 0, 0)

module.exports = Infobox
