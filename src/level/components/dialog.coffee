love = require 'lovejs'
bond = require 'bond'

class Dialog extends bond.components.Component
  constructor: ->
    @container = document.getElementById('dialog')
    @queue = new bond.Queue(@putMsg, @end)
    @visible = false
    @msg = null

    love.keyboard.on('pressed', @next)
    love.mouse.on('pressed', @next)
    window.addEventListener('touchstart', @next)

    love.eventify(this)

  clear: ->
    love.keyboard.removeListener('pressed', @next)
    love.mouse.removeListener('pressed', @next)
    window.removeEventListener('touchstart', @next)

    if @msg?
      @container.removeChild(@msg.el)

  next: =>
    if @visible and @msg?
      @msg.next()
      return false
    return true

  show: ->
    @visible = true
    @container.className = 'visible'

  hide: ->
    @visible = false
    @container.className = ''

  print: (name, text) ->
    @queue.push(new Message(name, text))

    if not @visible
      @start()

  start: ->
    @trigger('start')
    @show()
    @queue.start()

  end: =>
    @hide()
    @trigger('end')

  putMsg: (msg, done) =>
    prev = @msg
    @msg = msg

    if prev?
      @container.replaceChild(msg.el, prev.el)
    else
      @container.appendChild(msg.el)

    msg.on('end', done)
    msg.start()

module.exports = Dialog

class Message
  constructor: (name, text) ->
    @el = document.createElement('div')
    @el.className = "message #{name}"

    @visible = 0
    @text = text

    @interval = null

    love.eventify(this)

    @update()

  start: ->
    @trigger('start')
    @update()
    @interval = setInterval(@putc, 30)

  end: ->
    @trigger('end')

  skip: ->
    @visible = @text.length
    @update()

  next: ->
    if @visible is @text.length
      @end()
    else
      @skip()

  putc: =>
    @visible++
    @update()

  update: ->
    if @visible is @text.length
      clearInterval(@interval)

    @el.innerHTML = @getHtml()

  getHtml: ->
    """
    <span class="visible">#{@text.substr(0, @visible)}</span><span class="invisible">#{@text.substr(@visible)}</span>
    """



