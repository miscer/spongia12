Level = require './index'

class Tutorial extends Level
  init: ->
    super

    @player.keyboard.disallow('walking')
    @player.keyboard.disallow('jumping')

    @enemyZones.deactivate()

module.exports = Tutorial
