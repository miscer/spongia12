love = require 'lovejs'
bond = require 'bond'

game = require './game'

exports.start = (canvas) ->
  love.graphics.setCanvas(canvas)
  bond.gui.setContainer(canvas.parentNode)
  love.run()

love.load = ->
  game.menu()

love.update = (dt) ->
  if game.getCurrent()?
    game.getCurrent().update(dt)

love.draw = ->
  if game.getCurrent()?
    game.getCurrent().draw()
