love = require 'lovejs'
bond = require 'bond'

game = require './game'
state = require './state'

class Menu extends bond.components.Stage
  constructor: ->
    @asset = love.assets.addImage('images/menu/background.png')
    @audio = love.assets.addAudio('other/music/menu.mp3')
    love.assets.addImage('images/menu/buttons.png')

    @loaded = false

    @buttons =
      start: bond.gui.newButton('new game', 400, 416, 'new-game')
      load: bond.gui.newButton('load game', 400, 468, 'load-game')
      cards: bond.gui.newButton('cards album', 400, 516, 'cards-album')

    for name, button of @buttons
      button.on('click', @[name])

  init: ->
    @background = love.graphics.newImage(@asset)
    @audio = @audio.getContent()

    @loaded = true

  enter: ->
    for name, button of @buttons
      bond.gui.add(button)

    @audio.play()

  leave: ->
    for name, button of @buttons
      bond.gui.remove(button)

    @audio.pause()
    @audio.currentTime = 0

  draw: ->
    love.graphics.draw(@background, 0, 0)

  start: =>
    game.level(0)

  load: =>
    state.load()
    game.level(state.levels.current)

  cards: =>

module.exports = Menu
