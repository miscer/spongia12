Component = require '../src/component'
Container = require '../src/container'

class FooComponent extends Component

class BarComponent extends Component

class FooBarContainer extends Container
  createComponentFoo: ->
    new FooComponent

  createComponentBar: ->
    new BarComponent

describe 'Container', ->
  foobar = new FooBarContainer

  it 'should create foo component', ->
    foobar.should.have.property('foo')
    foobar.foo.should.be.instanceOf(FooComponent)

  it 'should create bar component', ->
    foobar.should.have.property('bar')
    foobar.bar.should.be.instanceOf(BarComponent)    

  it 'should set component parent', ->
    foobar.foo.should.have.property('parent', foobar)
    foobar.bar.should.have.property('parent', foobar)

  it 'should throw on non-existent component', ->
    ( ->
      abc = foobar.getComponent('abc')
    ).should.throw(/component abc/)
