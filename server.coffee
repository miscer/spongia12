express = require 'express'
mallow = require 'mallow'

app = express()
bundle = mallow.new(__dirname)

app.use app.router
app.use express.static('public')
app.use mallow.errorHandler

app.get '/game.js', bundle.server

app.listen 3000

console.log 'server running on http://localhost:3000/'
